package fr.sullygroup.metrowatch.activities;

import android.os.Bundle;

import fr.sullygroup.metrowatch.Constantes;

/**
 * Created by jocelyn.caraman on 09/10/2017.
 */

public class SplashActivity extends SplashActivityBase {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle b = getIntent().getExtras();
        if(b != null) {
            lat = b.getDouble("x",Constantes.DEFAULT_LATITUDE);
            longi = b.getDouble("y",Constantes.DEFAULT_LONGITUDE);
        }

        getData();
    }

    @Override
    protected boolean canLaunchMainActivity() {
        return isDataReady;
    }
}
