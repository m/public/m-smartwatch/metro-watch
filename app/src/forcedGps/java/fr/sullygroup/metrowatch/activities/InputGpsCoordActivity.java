package fr.sullygroup.metrowatch.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import fr.sullygroup.metrowatch.R;

/**
 * Created by jocelyn.caraman on 27/12/2017.
 */

public class InputGpsCoordActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input_gps_coord);

        EditText xCoord = findViewById(R.id.x_coord);
        EditText yCoord = findViewById(R.id.y_coord);

        ImageButton button  = findViewById(R.id.validate_button);
        button.setOnClickListener(v -> {
            if((!xCoord.getText().toString().isEmpty() && yCoord.getText().toString().isEmpty()) ||
                    (xCoord.getText().toString().isEmpty() && !yCoord.getText().toString().isEmpty())) {
                Toast.makeText(this,"Veuillez renseigner les deux champs ou aucun",Toast.LENGTH_SHORT).show();
            }
            else {
                Intent intent = new Intent(this, SplashActivity.class);
                if(!xCoord.getText().toString().isEmpty() && !yCoord.getText().toString().isEmpty()) {
                    intent.putExtra("x", Double.valueOf(xCoord.getText().toString()));
                    intent.putExtra("y", Double.valueOf(yCoord.getText().toString()));
                }
                startActivity(intent);
                finish();
            }
        });
    }
}
