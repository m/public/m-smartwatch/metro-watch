package fr.sullygroup.metrowatch.dagger.components;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjectionModule;
import dagger.android.AndroidInjector;
import fr.sullygroup.metrowatch.App;
import fr.sullygroup.metrowatch.dagger.modules.MainActivityModule;
import fr.sullygroup.metrowatch.dagger.modules.ServicesApplicationModule;
import fr.sullygroup.metrowatch.dagger.modules.SplashActivityModule;
import fr.sullygroup.metrowatch.dagger.modules.ViewModelModule;

/**
 * Created by jocelyn.caraman on 16/11/2017.
 */

@Singleton
@Component(modules = { ServicesApplicationModule.class, AndroidInjectionModule.class, ViewModelModule.class, MainActivityModule.class,SplashActivityModule.class})
public interface ApplicationComponent extends AndroidInjector<App> {
    @Component.Builder
    interface Builder {
        ApplicationComponent build();
        @BindsInstance
        ApplicationComponent.Builder application(App application);
    }
}