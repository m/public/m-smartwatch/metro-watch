package fr.sullygroup.metrowatch.dagger.modules;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.Executors;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import fr.sullygroup.metrowatch.App;
import fr.sullygroup.metrowatch.network.retrofit.LiveDataCallAdapterFactory;
import fr.sullygroup.metrowatch.gson.LigneTypeAdapter;
import fr.sullygroup.metrowatch.gson.ListTypeAdapterFactory;
import fr.sullygroup.metrowatch.gson.StopTypeAdapter;
import fr.sullygroup.metrowatch.gson.StopLigneTypeAdapter;
import fr.sullygroup.metrowatch.model.StopLigne;
import fr.sullygroup.metrowatch.model.Ligne;
import fr.sullygroup.metrowatch.model.MyObjectBox;
import fr.sullygroup.metrowatch.model.Stop;
import fr.sullygroup.metrowatch.network.retrofit.WebService;
import io.objectbox.BoxStore;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by jocelyn.caraman on 15/11/2017.
 */

@Module
public class ServicesApplicationModule {
    @Singleton
    @Provides
    BoxStore provideBoxStore(App app) {
        return MyObjectBox.builder().androidContext(app).build();
    }

    @Singleton
    @Provides
    WebService provideWebservice(){
        final GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapterFactory(ListTypeAdapterFactory.getListTypeAdapterFactory());
        gsonBuilder.registerTypeAdapter(Stop.class, new StopTypeAdapter());
        gsonBuilder.registerTypeAdapter(StopLigne.class, new StopLigneTypeAdapter());
        gsonBuilder.registerTypeAdapter(Ligne.class, new LigneTypeAdapter());
        Gson gson = gsonBuilder.create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://data.metromobilite.fr/")
                .addCallAdapterFactory(new LiveDataCallAdapterFactory())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .callbackExecutor(Executors.newSingleThreadExecutor())
                .build();

        return retrofit.create(WebService.class);
    }
}
