package fr.sullygroup.metrowatch.dagger.modules;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;
import fr.sullygroup.metrowatch.dagger.ApplicationViewModelFactory;
import fr.sullygroup.metrowatch.dagger.ViewModelKey;
import fr.sullygroup.metrowatch.viewmodel.ArretViewModel;

/**
 * Created by jocelyn.caraman on 15/11/2017.
 */

@Module
public abstract class ViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(ArretViewModel.class)
    abstract ViewModel bindArretViewModel(ArretViewModel arretViewModel);

    @Binds
    abstract ViewModelProvider.Factory bindViewModelFactory(ApplicationViewModelFactory factory);
}
