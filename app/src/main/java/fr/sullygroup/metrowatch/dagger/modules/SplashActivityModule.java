package fr.sullygroup.metrowatch.dagger.modules;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import fr.sullygroup.metrowatch.activities.SplashActivity;

/**
 * Created by jocelyn.caraman on 22/11/2017.
 */

@Module
public abstract class SplashActivityModule {

    @ContributesAndroidInjector
    abstract SplashActivity splashActivity();
}