package fr.sullygroup.metrowatch;

import android.support.annotation.NonNull;
import android.util.Log;

import java.util.List;

import fr.sullygroup.metrowatch.model.Arret;
import fr.sullygroup.metrowatch.model.Arret_;
import fr.sullygroup.metrowatch.model.Ligne;
import fr.sullygroup.metrowatch.model.Ligne_;
import fr.sullygroup.metrowatch.model.Passage;
import fr.sullygroup.metrowatch.model.Stop;
import fr.sullygroup.metrowatch.model.StopLigne;
import io.objectbox.BoxStore;
import retrofit2.Response;

/**
 * Created by jocelyn.caraman on 28/12/2017.
 */

public class Utils {

    public static boolean saveStopsFromWebservices(@NonNull BoxStore boxStore, @NonNull Response<List<Stop>> response, String tag) {
        List<Stop> stops = response.body();
        if(stops != null && stops.size() >= 0) {
            for (Stop s : stops) {
                String name = s.getName();
                boxStore.boxFor(Stop.class).attach(s);
                for (Ligne l : s.getTmpLignes()) {
                    Ligne ligneDB = boxStore.boxFor(Ligne.class).query().equal(Ligne_.id, l.getId()).build().findUnique();
                    if (ligneDB != null) {
                        s.getLignes().add(ligneDB);
                    }
                }
                s.setTmpLignes(null);
                Arret arret = boxStore.boxFor(Arret.class).query().equal(Arret_.nom, s.getName()).build().findUnique();
                if (arret != null) {
                    arret.getStops().add(s);
                    arret.getLignes().addAll(s.getLignes());
                    boxStore.boxFor(Arret.class).put(arret);
                }
                else {
                    arret = new Arret();
                    arret.setNom(name);
                    arret.getStops().add(s);
                    arret.getLignes().addAll(s.getLignes());
                    boxStore.boxFor(Arret.class).put(arret);
                }
            }
            return true;
        }
        else {
            Log.e(tag, "Pas de stops proches !");
            return false;
        }
    }

    public static void clearAllStores(BoxStore boxStore) {
        if(!boxStore.isClosed()) {
            boxStore.boxFor(Arret.class).removeAll();
            boxStore.boxFor(Stop.class).removeAll();
            boxStore.boxFor(StopLigne.class).removeAll();
            boxStore.boxFor(Ligne.class).removeAll();
            boxStore.boxFor(Passage.class).removeAll();
        }
    }
}
