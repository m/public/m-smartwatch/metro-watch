package fr.sullygroup.metrowatch.model;

import android.support.annotation.NonNull;

import java.util.Iterator;

import io.objectbox.annotation.Backlink;
import io.objectbox.annotation.Entity;
import io.objectbox.annotation.Id;
import io.objectbox.annotation.Index;
import io.objectbox.relation.ToMany;

/**
 * Created by jocelyn.caraman on 13/10/2017.
 */
@Entity
public class Arret implements Comparable{
    @Id
    private long id;
    @Index
    private String nom;
    private ToMany<Ligne> lignes;
    @Backlink
    private ToMany<Stop> stops;

    public Arret() {

    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public ToMany<Ligne> getLignes() {
        return lignes;
    }

    public void setLignes(ToMany<Ligne> lignes) {
        this.lignes = lignes;
    }

    public ToMany<Stop> getStops() {
        return stops;
    }

    public void setStops(ToMany<Stop> stops) {
        this.stops = stops;
    }

    @Override
    public String toString() {
        String s = "Arret{" +
                "id=" + id +
                ",nom='" + nom + '\'' +
                ", lignes=[";

        Iterator it = lignes.iterator();
        while(it.hasNext()){
            s += it.next().toString();
        }
        s+="], stops=[";
        Iterator it2 = stops.iterator();
        while(it2.hasNext()){
            s += it2.next().toString();
        }
        s+="]}";
        /*+
                ", stops=" + stops.toString() +
                '}';*/
        return s;
    }

    @Override
    public int compareTo(@NonNull Object o) {
        return nom.compareTo(((Arret)o).getNom());
    }
}
