package fr.sullygroup.metrowatch.model;

import android.text.TextUtils;

import java.lang.reflect.Array;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import io.objectbox.annotation.Backlink;
import io.objectbox.annotation.Convert;
import io.objectbox.annotation.Entity;
import io.objectbox.annotation.Id;
import io.objectbox.annotation.Transient;
import io.objectbox.converter.PropertyConverter;
import io.objectbox.relation.ToMany;
import io.objectbox.relation.ToOne;

/**
 * Created by jocelyn.caraman on 13/10/2017.
 */
@Entity
public class StopLigne {
    @Id
    private long id;
    @Transient
    private String nomLigne;
    private ToOne<Ligne> ligne;
    private ToOne<Stop> stop;
    @Backlink
    private ToMany<Passage> passages;
    private int codeDirection;
    private String libelleDirection;
    private boolean terminus;
    private long timestamp;

    public StopLigne() {

    }

    public int getCodeDirection() {
        return codeDirection;
    }

    public void setCodeDirection(int codeDirection) {
        this.codeDirection = codeDirection;
    }

    public String getLibelleDirection() {
        return libelleDirection;
    }

    public void setLibelleDirection(String libelleDirection) {
        this.libelleDirection = libelleDirection;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public ToOne<Ligne> getLigne() {
        return ligne;
    }

    public void setLigne(ToOne<Ligne> ligne) {
        this.ligne = ligne;
    }

    public ToOne<Stop> getStop() {
        return stop;
    }

    public void setStop(ToOne<Stop> stop) {
        this.stop = stop;
    }

    public ToMany<Passage> getPassages() {
        return passages;
    }

    public boolean isTerminus() {
        return terminus;
    }

    public void setTerminus(boolean terminus) {
        this.terminus = terminus;
    }

    public String getNomLigne() {
        return nomLigne;
    }

    public void setNomLigne(String nomLigne) {
        this.nomLigne = nomLigne;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        StopLigne stopLigne = (StopLigne) o;

        return id == stopLigne.id;
    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }

    @Override
    public String toString() {
        return "StopLigne{" +
                "ligne=" + ligne +
                ", stop=" + stop +
                ", passages=" + passages.toString() +
                '}';
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }
}
