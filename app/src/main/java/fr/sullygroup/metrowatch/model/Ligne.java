package fr.sullygroup.metrowatch.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import io.objectbox.annotation.Convert;
import io.objectbox.annotation.Entity;
import io.objectbox.annotation.Id;
import io.objectbox.annotation.Index;
import io.objectbox.converter.PropertyConverter;

/**
 * Created by jocelyn.caraman on 13/10/2017.
 */

@Entity
public class Ligne implements Comparable, Parcelable{
    @Id
    private long numId;
    @Index
    private String id;
    @Convert(converter = Stop.ReseauConverter.class, dbType = Integer.class)
    private Stop.Reseau reseau;
    @Convert(converter = TypeConverter.class, dbType = Integer.class)
    private Type type;


    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getTextColor() {
        return textColor;
    }

    public void setTextColor(String textColor) {
        this.textColor = textColor;
    }

    private String color;

    private String textColor;

    public Ligne() {
    }

    public Ligne(String id, String reseau) {
        this.id = id;
        this.reseau = Stop.Reseau.valueOf(reseau);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Stop.Reseau getReseau() {
        return reseau;
    }

    public void setReseau(Stop.Reseau reseau) {
        this.reseau = reseau;
    }

    @Override
    public int compareTo(@NonNull Object o) {
        return id.compareTo(((Ligne)o).getId());
    }

    @Override
    public String toString() {
        return "Ligne{" +
                "numid=" + numId +
                ",id='" + id + '\'' +
                ", reseau=" + reseau +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Ligne ligne = (Ligne) o;

        return id.equals(ligne.id);

    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    public long getNumId() {
        return numId;
    }

    public void setNumId(long numId) {
        this.numId = numId;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }


    public enum Type {
        INCONNU(0),SEM(1),C38(2),CHRONO(3),FLEXO(4),TRAM(5),PROXIMO(6);

        final int id;

        Type(int id) {
            this.id = id;
        }
    }

    public static class TypeConverter implements PropertyConverter<Type, Integer> {
        @Override
        public Type convertToEntityProperty(Integer databaseValue) {
            if (databaseValue == null) {
                return null;
            }
            for (Type type : Type.values()) {
                if (type.id == databaseValue) {
                    return type;
                }
            }
            return Type.INCONNU;
        }

        @Override
        public Integer convertToDatabaseValue(Type entityProperty) {
            return entityProperty == null ? null : entityProperty.id;
        }
    }

    /**
     * PARCELABLE
     */

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeLong(numId);
        out.writeString(id);
        out.writeString(reseau.name());
    }

    public static final Parcelable.Creator<Ligne> CREATOR
            = new Parcelable.Creator<Ligne>() {
        public Ligne createFromParcel(Parcel in) {
            return new Ligne(in);
        }

        public Ligne[] newArray(int size) {
            return new Ligne[size];
        }
    };

    private Ligne(Parcel in) {
        numId = in.readInt();
        id = in.readString();
        reseau = Stop.Reseau.valueOf(in.readString());
    }
}
