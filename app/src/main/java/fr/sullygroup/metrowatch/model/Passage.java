package fr.sullygroup.metrowatch.model;

import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import io.objectbox.annotation.Entity;
import io.objectbox.annotation.Id;
import io.objectbox.relation.ToOne;

/**
 * Created by jocelyn.caraman on 28/11/2017.
 */

@Entity
public class Passage {
    @Id
    public long id;
    public boolean realtime;
    public long horaireEnSeconde;
    public ToOne<StopLigne> stopLigne;

    public String getHorairePassage() {
        try {
            long allMinutes = horaireEnSeconde / 60;
            int hour = (int) allMinutes / 60;
            int minutes = (int) allMinutes;
            if(hour != 0)
                minutes = (int) allMinutes % 60;
            Calendar passage = Calendar.getInstance();
            passage.set(Calendar.MILLISECOND,0);
            passage.set(Calendar.SECOND,0);
            passage.set(Calendar.MINUTE,minutes);
            passage.set(Calendar.HOUR_OF_DAY,hour);

            Calendar now = Calendar.getInstance();
            now.set(Calendar.MILLISECOND,0);
            now.set(Calendar.SECOND,0);

            int compareDate = now.compareTo(passage);
            SimpleDateFormat formater = new SimpleDateFormat("HH:mm", Locale.FRANCE);
            SimpleDateFormat formater2 = new SimpleDateFormat("HH:mm:ss:SSSS", Locale.FRANCE);
            Log.d("Passage", "getHorairePassage: now " + formater2.format(now.getTime()) + " passage " + formater2.format(passage.getTime()));
            if(compareDate < 0)
            {
                // passage postérieur
                // test si le transport passe dans moins d'1h ou pas
                Calendar nowPlus1h = Calendar.getInstance();
                nowPlus1h.add(Calendar.HOUR_OF_DAY,1);
                int compareDate1h = nowPlus1h.compareTo(passage);
                if(compareDate1h < 0)
                    // plus d'1h d'attente
                    return formater.format(passage.getTime());
                else
                {
                    // moins ou égal à 1h d'attente
                    long duration  = passage.getTimeInMillis() - now.getTimeInMillis();
                    long diffInMinutes = TimeUnit.MILLISECONDS.toMinutes(duration);
                    if(diffInMinutes == 0 )
                        return "Now";
                    else
                        return diffInMinutes + "min.";
                }
            }
            else if(compareDate == 0)
                // NOW
                return "Now";
            else
                // passage antérieur
                return "Raté!";
        } catch (Exception e) {
            e.printStackTrace();
            return "N/A";
        }
    }
}
