package fr.sullygroup.metrowatch.model;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

import io.objectbox.annotation.Backlink;
import io.objectbox.annotation.Convert;
import io.objectbox.annotation.Entity;
import io.objectbox.annotation.Id;
import io.objectbox.annotation.Transient;
import io.objectbox.converter.PropertyConverter;
import io.objectbox.relation.ToMany;
import io.objectbox.relation.ToOne;

/**
 * Created by jocelyn.caraman on 13/10/2017.
 */
@Entity
public class Stop implements Comparable{
    @Id(assignable = true)
    private long id;

    @Transient
    private String name;
    private ToOne<Arret> arret;
    @Convert(converter = Stop.ReseauConverter.class, dbType = Integer.class)
    private Reseau reseau;
    @Transient
    private List<Ligne> tmpLignes;
    private ToMany<Ligne> lignes;
    @Backlink
    private ToMany<StopLigne> stopLignes;

    public Stop() {
    }

    public Stop(String id, Reseau reseau) {
        this.id = Long.valueOf(id);
        this.reseau = reseau;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public ToOne<Arret> getArret() {
        return arret;
    }

    public void setArret(ToOne<Arret> arret) {
        this.arret = arret;
    }

    public Reseau getReseau() {
        return reseau;
    }

    public void setReseau(Reseau reseau) {
        this.reseau = reseau;
    }

    public ToMany<Ligne> getLignes() {
        return lignes;
    }

    public void setLignes(ToMany<Ligne> lignes) {
        this.lignes = lignes;
    }

    public ToMany<StopLigne> getStopLignes() {
        return stopLignes;
    }

    public void setStopLignes(ToMany<StopLigne> stopLignes) {
        this.stopLignes = stopLignes;
    }

    public String getIdForJson() {
        return reseau + ":" + id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Ligne> getTmpLignes() {
        if(tmpLignes == null) {
            tmpLignes = new ArrayList<>();
        }
        return tmpLignes;
    }

    public void setTmpLignes(List<Ligne> tmpLignes) {
        this.tmpLignes = tmpLignes;
    }


    @Override
    public String toString() {
        /*return "Stop{" +
                "id='" + id + '\'' +
                ", arret=" + (arret.getTarget() != null ?arret.getTarget().toString():"null") +
                ", lignes=" + lignes.toString() +
                ", stopLignes=" + /*stopLignes.toString() +*/
                //'}';
        return"1";
    }

    @Override
    public int compareTo(@NonNull Object o) {
        return Long.compare(id,((Stop)o).getId());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Stop stop = (Stop) o;

        return id == stop.id;

    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }

    public enum Reseau {
        INCONU(0),SEM(1),C38(2),SNC(3),TPV(4),GSV(5),FUN(6),BUL(7);

        final int id;

        Reseau(int id) {
            this.id = id;
        }
    }

    public static class ReseauConverter implements PropertyConverter<Reseau, Integer> {
        @Override
        public Reseau convertToEntityProperty(Integer databaseValue) {
            if (databaseValue == null) {
                return null;
            }
            for (Reseau role : Reseau.values()) {
                if (role.id == databaseValue) {
                    return role;
                }
            }
            return Reseau.INCONU;
        }

        @Override
        public Integer convertToDatabaseValue(Reseau entityProperty) {
            return entityProperty == null ? null : entityProperty.id;
        }
    }
}
