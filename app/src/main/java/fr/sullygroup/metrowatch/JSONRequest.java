package fr.sullygroup.metrowatch;

/**
 * Created by jocelyn.caraman on 30/10/2017.
 */

public class JSONRequest {
    public String type;
    public String[] args;

    public JSONRequest(String type,String... mArgs) {
        this.type = type;
        if(mArgs.length > 0)
            args = mArgs;
    }
}
