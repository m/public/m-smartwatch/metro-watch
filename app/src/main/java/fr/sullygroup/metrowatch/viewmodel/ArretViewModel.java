package fr.sullygroup.metrowatch.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModel;
import android.os.Handler;
import android.util.Log;

import java.util.List;

import javax.inject.Inject;

import fr.sullygroup.metrowatch.Constantes;
import fr.sullygroup.metrowatch.model.Arret;
import fr.sullygroup.metrowatch.model.StopLigne;
import fr.sullygroup.metrowatch.model.Ligne;
import fr.sullygroup.metrowatch.network.Resource;
import fr.sullygroup.metrowatch.repository.ArretRepository;
import io.objectbox.android.ObjectBoxLiveData;

/**
 * Created by jocelyn.caraman on 14/11/2017.
 */

public class ArretViewModel extends ViewModel {
    private ArretRepository arretRepo;
    public ObjectBoxLiveData<Arret> arrets;
    public LiveData<Arret> arret;
    public LiveData<Ligne> ligne;

    private LiveData<Resource<List<StopLigne>>> horairesSource;
    public MediatorLiveData<Resource<List<StopLigne>>> horaires;

    @Inject
    public ArretViewModel(ArretRepository mArretRepo) {
        this.arretRepo = mArretRepo;
    }

    public void init() {
        final Handler handler = new Handler();
        arrets = arretRepo.getArrets();
        arret = Transformations.map(arrets, arrets -> {
            if(!arrets.isEmpty()) return arrets.get(0);
            return null;
        });
        ligne = Transformations.map(arret, arret -> {
            if(arret != null) return  arret.getLignes().get(0);
            return null;
        });

        horaires = new MediatorLiveData<>();

        ligne.observeForever(newLigne -> {
            handler.removeCallbacksAndMessages(null);
            if(newLigne != null) {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        Log.d("handler","MAJ horaires");
                        updateHoraires(ligne.getValue());
                        handler.postDelayed(this, Constantes.DELAI_ACTUALISATION_HORAIRES_AUTO);
                    }
                });
            }
        });
    }

    public ObjectBoxLiveData<Arret> getArret() {
        return arrets;
    }

    private void updateHoraires(Ligne ligne) {
        if(ligne != null) {
            if(horairesSource != null)
                horaires.removeSource(horairesSource);
            horairesSource = arretRepo.loadHoraires(ligne);
            horaires.addSource(horairesSource,value -> horaires.setValue(value));
        }
    }
}
