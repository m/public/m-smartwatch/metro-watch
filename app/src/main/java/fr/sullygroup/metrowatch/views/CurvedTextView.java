package fr.sullygroup.metrowatch.views;

/**
 * Created by jocelyn.caraman on 09/10/2017.
 */

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;

import android.graphics.Point;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;

import fr.sullygroup.metrowatch.R;

public class CurvedTextView extends View {
    private String MY_TEXT = "Gare Part-Dieu - Vivier Merle";
    private Path mArc;
    private Paint mPaintText;
    private float textSize = 32f;
    private int screenX;
    private int screenY;
    private float recSize;
    private boolean isOk = false;

    public CurvedTextView(Context context) {
        super(context);

        mArc = new Path();
        RectF oval = new RectF(50,100,200,250);
        mArc.addArc(oval, -90, 200);
        mPaintText = new Paint(Paint.ANTI_ALIAS_FLAG);
        mPaintText.setStyle(Paint.Style.FILL_AND_STROKE);
        mPaintText.setColor(Color.WHITE);
        mPaintText.setTextSize(20f);

    }

    public CurvedTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.CurvedTextView,
                0, 0);

        try {
            String text = a.getString(R.styleable.CurvedTextView_text);
            MY_TEXT = text != null ? text : "Oops";
        } finally {
            a.recycle();
        }

        Display display = context.getSystemService(WindowManager.class).getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        screenX = size.x;
        screenY = size.y;

        recSize = screenX - (2*textSize);

        mArc = new Path();

        mPaintText = new Paint(Paint.ANTI_ALIAS_FLAG);
        mPaintText.setStyle(Paint.Style.FILL_AND_STROKE);
        mPaintText.setColor(Color.WHITE);
        mPaintText.setTextSize(textSize);

    }

    @Override
    protected void onDraw(Canvas canvas) {
        if(!isOk && MY_TEXT != null) {
            RectF oval = new RectF(textSize,textSize,textSize+recSize,textSize+recSize);
            float tmp = (float) (0.022 * (float) Math.pow((double)MY_TEXT.length(),2) - 2.9273 * MY_TEXT.length() + 263.41);
            mArc.addArc(oval, tmp, 180f);
            isOk = true;
        }
        canvas.drawTextOnPath(MY_TEXT, mArc, 0, 0, mPaintText);
    }

    public String getText() {
        return MY_TEXT;
    }

    public void setText(String text) {
        MY_TEXT = text;
        invalidate();
        requestLayout();
    }
}