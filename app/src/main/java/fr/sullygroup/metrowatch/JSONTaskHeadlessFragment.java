package fr.sullygroup.metrowatch;

/**
 * Created by jocelyn.caraman on 10/10/2017.
 */

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.TimeUnit;

public class JSONTaskHeadlessFragment extends Fragment {

    public static final String TAG_HEADLESS_FRAGMENT = "headless_fragment";

    public static final String REQUEST_GET_DESCRIPTION_LINES = "desc";
    public static final String REQUEST_GET_LINES_NEAR = "lines";
    public static final String REQUEST_GET_STOPTIMES = "stoptimes";
    private static final String TAG = "JSONTaskHFrag";

    public interface TaskStatusCallback {
        void onPreExecute();

        void onProgressUpdate(int progress);

        void onPostExecute(String typeRequest, String json);

        void onCancelled();
    }

    ArrayBlockingQueue<JSONRequest> queue;
    TaskStatusCallback mStatusCallback;
    List<BackgroundTask> mBackgroundTasks;
    int count = 0;

    public JSONTaskHeadlessFragment() {
        mBackgroundTasks = new ArrayList<>();
        queue = new ArrayBlockingQueue<JSONRequest>(5);
        Thread dispatchJSONRequest = new Thread(new DispatchJSONRequest());
        dispatchJSONRequest.start();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mStatusCallback = (TaskStatusCallback) context;
        Log.d(TAG,"onAttach");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mStatusCallback = null;
        Log.d(TAG,"onDetach");
    }

    private class DispatchJSONRequest implements Runnable {

        @Override
        public void run() {
            try {
                while (true) {
                    JSONRequest req = queue.take();
                    if (req != null) {
                        switch (req.type) {
                            case REQUEST_GET_DESCRIPTION_LINES:
                                break;
                            case REQUEST_GET_LINES_NEAR:
                                getLinesNear(req.args[0],req.args[1]);
                                break;
                            case REQUEST_GET_STOPTIMES:
                                getStoptimes(req.args[0]);
                                break;
                        }
                    }
                }

            }  catch (InterruptedException e) {
                System.out.println("[" + Thread.currentThread().getName() +  "] je m'arrête") ;
            }
        }
    }

    private class BackgroundTask extends AsyncTask<String, Void, String> {

        private String type;

        @Override
        protected void onPreExecute() {
            if (mStatusCallback != null)
                mStatusCallback.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            int progress = 0;
            try {
                if(params[0] == null)
                    return null;
                type = params[0];
                String urlString = "";
                switch (type){
                    case REQUEST_GET_DESCRIPTION_LINES:
                        urlString = getString(R.string.url_get_description_lines);
                        break;
                    case REQUEST_GET_LINES_NEAR:
                        if(params.length != 3)
                            return null;
                        urlString = getString(R.string.url_get_lines_near,params[1],params[2]);
                        break;
                    case REQUEST_GET_STOPTIMES:
                        if(params.length != 2)
                            return null;
                        urlString = getString(R.string.url_get_stoptimes,params[1]);
                        break;
                    default:
                        return null;
                }

                URL url = new URL(urlString);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.connect();
                InputStream inputStream = connection.getInputStream();
                String result = InputStreamOperations.InputStreamToString(inputStream);

                Log.d(TAG,result);

                return result;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String json) {
            if (mStatusCallback != null)
                mStatusCallback.onPostExecute(type, json);
        }

        /*@Override
        protected void onProgressUpdate(Integer... values) {
            if (mStatusCallback != null)
                mStatusCallback.onProgressUpdate(values[0]);
        }*/

        @Override
        protected void onCancelled(String result) {
            if (mStatusCallback != null)
                mStatusCallback.onCancelled();
        }

    }

    private BackgroundTask startBackgroundTask() {
        if (count < 5) {
            BackgroundTask bg = new BackgroundTask();
            mBackgroundTasks.add(bg);
            return bg;
        }
        return null;
    }

    public void getDescriptionLines() {
        BackgroundTask bg = startBackgroundTask();
        if(bg != null) {
            bg.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, REQUEST_GET_DESCRIPTION_LINES);
        }
    }

    public void getStoptimes(String idStop) {
        BackgroundTask bg = startBackgroundTask();
        if(bg != null) {
            bg.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, REQUEST_GET_STOPTIMES, idStop);
        }
    }

    public void getLinesNear(String x, String y) {
        BackgroundTask bg = startBackgroundTask();
        if(bg != null) {
            bg.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, REQUEST_GET_LINES_NEAR, x, y);
        }
    }

    public void addRequest(final JSONRequest req) {
        ((Runnable) () -> {
            try {
                boolean done = queue.offer(req, 1, TimeUnit.SECONDS);
                Log.d("queue", done + "");
            } catch (InterruptedException e) {
                System.out.println("[" + Thread.currentThread().getName() + "] je m'arrête");
            }
        }).run();
    }

    public void updateExecutingStatus(boolean isExecuting) {
        //this.isTaskExecuting = isExecuting;
    }

    private static class InputStreamOperations {

        /**
         * @param in : buffer with the php result
         * @param bufSize : size of the buffer
         * @return : the string corresponding to the buffer
         */
        public static String InputStreamToString (InputStream in, int bufSize) {
            final StringBuilder out = new StringBuilder();
            final byte[] buffer = new byte[bufSize];
            try {
                for (int ctr; (ctr = in.read(buffer)) != -1;) {
                    out.append(new String(buffer, 0, ctr));
                }
            } catch (IOException e) {
                throw new RuntimeException("Cannot convert stream to string", e);
            }
            // On retourne la chaine contenant les donnees de l'InputStream
            return out.toString();
        }

        /**
         * @param in : buffer with the php result
         * @return : the string corresponding to the buffer
         */
        public static String InputStreamToString (InputStream in) {
            // On appelle la methode precedente avec une taille de buffer par defaut
            return InputStreamToString(in, 1024);
        }

    }
}