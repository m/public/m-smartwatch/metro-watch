package fr.sullygroup.metrowatch.databinding;

import android.content.res.ColorStateList;
import android.databinding.BindingAdapter;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.v7.widget.AppCompatTextView;
import android.widget.ImageView;
import android.widget.TextView;

import fr.sullygroup.metrowatch.model.Ligne;

/**
 * Created by jocelyn.caraman on 23/11/2017.
 */

public class DataBindingAdapters {

    @BindingAdapter("android:src")
    public static void setImageUri(ImageView view, String imageUri) {
        if (imageUri == null) {
            view.setImageURI(null);
        } else {
            view.setImageURI(Uri.parse(imageUri));
        }
    }

    @BindingAdapter("android:src")
    public static void setImageUri(ImageView view, Uri imageUri) {
        view.setImageURI(imageUri);
    }

    @BindingAdapter("android:src")
    public static void setImageDrawable(ImageView view, Drawable drawable) {
        view.setImageDrawable(drawable);
    }

    @BindingAdapter("android:src")
    public static void setImageResource(ImageView imageView, int resource){
        imageView.setImageResource(resource);
    }

    @BindingAdapter("android:tint")
    public static void setImageTintColor(ImageView view, String sColor) {
        view.setImageTintList(ColorStateList.valueOf(Color.parseColor("#"+sColor)));
    }

    @BindingAdapter("android:textColor")
    public static void setTextColor(AppCompatTextView textView, String sColor) {
        textView.setTextColor(Color.parseColor("#"+sColor));
    }
}