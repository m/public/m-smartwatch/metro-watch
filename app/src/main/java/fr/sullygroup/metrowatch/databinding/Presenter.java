package fr.sullygroup.metrowatch.databinding;

import android.util.Log;
import android.view.View;

import fr.sullygroup.metrowatch.R;
import fr.sullygroup.metrowatch.activities.MainActivity;
import fr.sullygroup.metrowatch.model.Ligne;

/**
 * Created by jocelyn.caraman on 31/10/2017.
 */

public class Presenter {
    MainActivity activity;

    public Presenter(MainActivity activity) {
        this.activity = activity;
    }

    public void onClick(View view, Ligne ligne) {
        if(activity != null)
        {
            activity.afficherLigne(ligne);
            Log.d("dialog","hello");
            activity.dismissDiallog();
        }
    }

    public int getRessourceDrawableForLogo(Ligne ligne) {
        switch(ligne.getType()) {
            case C38 :
                return R.drawable.rectangle;
            case PROXIMO:
            case FLEXO:
                return R.drawable.square;
            case TRAM:
            case CHRONO:
                return R.drawable.circle;
            default: return R.drawable.circle;
        }
    }

}
