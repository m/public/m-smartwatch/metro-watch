package fr.sullygroup.metrowatch;

/**
 * Created by jocelyn.caraman on 08/12/2017.
 */

public class Constantes {
    // 60s
    public final static int DELAI_ACTUALISATION_HORAIRES_AUTO = 60 * 1000;

    public final static double DEFAULT_LATITUDE = 45.185344d;
    public final static double DEFAULT_LONGITUDE = 5.731791d;
}
