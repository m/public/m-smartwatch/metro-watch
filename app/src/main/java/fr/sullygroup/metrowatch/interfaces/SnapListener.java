package fr.sullygroup.metrowatch.interfaces;

import android.view.View;

/**
 * Created by jocelyn.caraman on 04/12/2017.
 */

public interface SnapListener {
    void onSnap(View view);
}
