package fr.sullygroup.metrowatch;

/**
 * Created by jocelyn.caraman on 16/10/2017.
 */

import android.app.Activity;
import android.app.Application;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasActivityInjector;
import fr.sullygroup.metrowatch.dagger.components.DaggerApplicationComponent;
import io.objectbox.BoxStore;
import io.objectbox.android.AndroidObjectBrowser;


public class App extends Application implements HasActivityInjector {

    public static final String TAG = "App";

    @Inject
    DispatchingAndroidInjector<Activity> dispatchingActivityInjector;

    public App() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
        DaggerApplicationComponent.builder().application(this).build().inject(this);
    }

    @Override
    public AndroidInjector<Activity> activityInjector() {
        return dispatchingActivityInjector;
    }
}