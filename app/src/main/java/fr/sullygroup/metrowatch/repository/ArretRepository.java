package fr.sullygroup.metrowatch.repository;

import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import fr.sullygroup.metrowatch.model.Arret;
import fr.sullygroup.metrowatch.model.Passage;
import fr.sullygroup.metrowatch.model.StopLigne;
import fr.sullygroup.metrowatch.model.Ligne;
import fr.sullygroup.metrowatch.model.Ligne_;
import fr.sullygroup.metrowatch.model.Stop;
import fr.sullygroup.metrowatch.model.StopLigne_;
import fr.sullygroup.metrowatch.model.Stop_;
import fr.sullygroup.metrowatch.network.ApiResponse;
import fr.sullygroup.metrowatch.network.NetworkBoundResource;
import fr.sullygroup.metrowatch.network.Resource;
import fr.sullygroup.metrowatch.network.retrofit.WebService;
import io.objectbox.BoxStore;
import io.objectbox.Property;
import io.objectbox.android.ObjectBoxLiveData;

/**
 * Created by jocelyn.caraman on 14/11/2017.
 */

@Singleton
public class ArretRepository {
    private WebService webservice;
    private BoxStore box;

    @Inject
    public ArretRepository(WebService webservice, BoxStore mBox) {
        this.webservice = webservice;
        this.box = mBox;
    }

    public ObjectBoxLiveData<Arret> getArrets() {
        return new ObjectBoxLiveData<>(box.boxFor(Arret.class).query().build());
    }

    private ObjectBoxLiveData<StopLigne> getHoraires(final Ligne ligne) {
        return new ObjectBoxLiveData<>(box.boxFor(StopLigne.class).query().filter(entity -> {
            Ligne ltmp = entity.getLigne().getTarget();
            if (ltmp == null) {
                Log.e("ArretRepo", "target null");
                return false;
            } else
                return ltmp.equals(ligne);
        }).build());
    }

    public LiveData<Resource<List<StopLigne>>> loadHoraires(final Ligne ligne) {
        final List<Stop> stops = box.boxFor(Stop.class).query().eager(Stop_.lignes).filter(entity ->
            entity.getLignes().contains(ligne)).build().find();
        Log.d("loadHoraires",ligne.getId());

        return new NetworkBoundResource<List<StopLigne>,List<StopLigne>>() {
            @Override
            protected void saveCallResult(@NonNull List<StopLigne> item) {
                for(StopLigne sl : item) {
                    if(sl.getNomLigne() != null) {
                        Ligne ligneDB = box.boxFor(Ligne.class).query().equal(Ligne_.id, sl.getNomLigne()).build().findUnique();
                        if(ligneDB != null) {
                            StopLigne stopLigneDB = box.boxFor(StopLigne.class).query().eager(StopLigne_.ligne).equal(StopLigne_.ligneId,ligneDB.getNumId()).equal(StopLigne_.libelleDirection,sl.getLibelleDirection())
                                    .equal(StopLigne_.stopId,sl.getStop().getTargetId()).build().findUnique();
                            if(stopLigneDB != null) {
                                // StopLigne déjà existant : MAJ Passages
                                Log.d("MAJ","MAJ StopLigne");
                                stopLigneDB.getPassages().addAll(sl.getPassages());
                                box.boxFor(StopLigne.class).put(stopLigneDB);
                            }
                            else {
                                sl.getLigne().setTargetId(ligneDB.getNumId());
                                box.boxFor(StopLigne.class).put(sl);
                            }
                        }
                    }
                }
            }

            @Override
            protected @ShouldFetch int shouldFetch(@Nullable List<StopLigne> data) {
                if(data == null || data.size() == 0) {
                    Log.d("shouldFetch","true ALL");
                    return SHOULD_FETCH_ALL;
                }

                long now = Calendar.getInstance().getTimeInMillis();
                boolean fetchPassagesOnly = false;
                for(StopLigne sl : data) {
                    if(now - sl.getTimestamp() > 60 * 1000)
                        fetchPassagesOnly = true;
                }
                if(fetchPassagesOnly) {
                    for(StopLigne sl : data) {
                        box.boxFor(Passage.class).remove(sl.getPassages());
                        sl.getPassages().clear();
                    }
                    Log.d("shouldFetch","true ONLY PASSSAGES");
                    return SHOULD_FETCH_PASSAGES;
                }
                Log.d("shouldFetch","false");
                return SHOULD_NOT_FETCH;
            }

            @NonNull @Override
            protected LiveData<List<StopLigne>> loadFromDb() {
                return getHoraires(ligne);
            }

            @NonNull @Override
            protected List<LiveData<ApiResponse<List<StopLigne>>>> createCall() {
                List<LiveData<ApiResponse<List<StopLigne>>>> apiCalls = new ArrayList<>(2);
                for(Stop stop : stops)
                    apiCalls.add(webservice.getHoraires(String.valueOf(stop.getId()),stop.getReseau().toString()));
                return apiCalls;
            }

            @Override
            protected <T> void onFetchFailed(ApiResponse<T> response) {
                Log.e("Retro",response.errorMessage);
            }
        }.getAsLiveData();
    }
}
