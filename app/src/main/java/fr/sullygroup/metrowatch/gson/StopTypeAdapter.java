package fr.sullygroup.metrowatch.gson;

import android.util.Log;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;

import fr.sullygroup.metrowatch.model.*;
import io.objectbox.BoxStore;
import io.objectbox.relation.ToMany;

/**
 * Created by jocelyn.caraman on 13/10/2017.
 */

public class StopTypeAdapter extends TypeAdapter {
    private static String TAG = "StopTypeAdapt";

    public StopTypeAdapter() {
    }

    @Override
    public Stop read(final JsonReader in) throws IOException {

        Stop stop = new Stop();
        Ligne ligne;

        String[] split;
        String id;
        String nomArret = "";

        in.beginObject();
        while (in.hasNext()) {
            switch (in.nextName()) {
                case "name":
                    nomArret = in.nextString().split(", ")[1];
                    stop.setName(nomArret);
                    break;
                case "lines":
                    in.beginArray();
                    while (in.hasNext()) {
                        split = in.nextString().split(":");
                        ligne = new Ligne(split[1],split[0]);
                        ligne.setNumId(0L);
                        stop.getTmpLignes().add(ligne);
                    }
                    in.endArray();
                    break;
                case "id":
                    split = in.nextString().split(":");
                    stop.setReseau(Stop.Reseau.valueOf(split[0]));
                    stop.setId(Long.valueOf(split[1]));
                    break;
                default:
                    in.nextString();
                    break;
            }
        }
        in.endObject();
        return stop;
    }

    @SuppressWarnings("unchecked")
    public <T> void addAllUnique(ToMany<T> list, ToMany<T> listToAdd) {
        for (Object o : listToAdd) {
            T t = (T) o;
            if (!list.contains(o)) {
                list.add(t);

            }
        }
    }

    @Override
    public void write(JsonWriter out, Object value) throws IOException {
        //todo
    }
}
