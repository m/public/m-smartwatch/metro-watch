package fr.sullygroup.metrowatch.gson;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;
import java.util.Calendar;

import fr.sullygroup.metrowatch.model.StopLigne;
import fr.sullygroup.metrowatch.model.Passage;

/**
 * Created by jocelyn.caraman on 16/10/2017.
 */

public class StopLigneTypeAdapter extends TypeAdapter {

    private static String TAG = "HoraireTypeAdapt" ;

    @Override
    public void write(JsonWriter out, Object value) throws IOException {

    }

    @Override
    public StopLigne read(JsonReader in) throws IOException {
        StopLigne stopLigne = new StopLigne();
        stopLigne.setTimestamp(Calendar.getInstance().getTimeInMillis());
        Passage passage = null;
        boolean realtime = false;
        long scheduledDeparture = -1;
        long realtimeDeparture = -1;
        String[] split;
        int countPassages = 0;

        in.beginObject();
        while (in.hasNext()) {
            switch (in.nextName()) {
                case "pattern":
                    in.beginObject();
                    while (in.hasNext()) {
                        switch (in.nextName()) {
                            case "id":
                                stopLigne.setNomLigne(in.nextString().split(":")[1]);
                                break;
                            case "desc":
                                in.nextString();
                                break;
                            case "dir":
                                stopLigne.setCodeDirection(in.nextInt());
                                break;
                            case "shortDesc":
                                stopLigne.setLibelleDirection(in.nextString());
                                break;
                        }
                    }
                    in.endObject();
                    break;
                case "times":
                    in.beginArray();
                    while (in.hasNext()) {
                        in.beginObject();
                        while (in.hasNext()) {
                            switch (in.nextName()) {
                                case "stopId":
                                    split = in.nextString().split(":");
                                    stopLigne.getStop().setTargetId(Long.valueOf(split[1]));
                                    break;
                                case "scheduledDeparture":
                                    try {
                                        scheduledDeparture = in.nextLong();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        scheduledDeparture = -1;
                                    }
                                    break;
                                case "realtimeDeparture":
                                    try {
                                        realtimeDeparture = in.nextLong();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        realtimeDeparture = -1;
                                    }
                                    break;
                                case "realtime":
                                    realtime = in.nextBoolean();
                                    break;
                                default:
                                    in.skipValue();
                                    break;
                            }
                        }
                        if(countPassages < 2) {
                            //passages
                            if(realtimeDeparture == -1 && scheduledDeparture == -1){
                                stopLigne.setTerminus(true);
                            }
                            else {
                                passage = new Passage();
                                passage.realtime = realtime;
                                stopLigne.setTerminus(false);
                                if(realtime){
                                    if(realtimeDeparture != -1){
                                        passage.horaireEnSeconde = realtimeDeparture;
                                    }
                                    else{
                                        passage.horaireEnSeconde = scheduledDeparture;
                                    }
                                }
                                else {
                                    if(scheduledDeparture != -1){
                                        passage.horaireEnSeconde = scheduledDeparture;
                                    }
                                }
                                stopLigne.getPassages().add(passage);
                            }
                            countPassages++;
                        }
                        in.endObject();
                    }
                    in.endArray();
                    break;
            }
        }
        in.endObject();
        return stopLigne;
    }
}
