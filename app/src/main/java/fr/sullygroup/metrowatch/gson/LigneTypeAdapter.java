package fr.sullygroup.metrowatch.gson;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;

import fr.sullygroup.metrowatch.model.Ligne;
import fr.sullygroup.metrowatch.model.Stop;
import io.objectbox.relation.ToMany;

/**
 * Created by jocelyn.caraman on 23/11/2017.
 */

public class LigneTypeAdapter extends TypeAdapter {
    private static String TAG = "StopTypeAdapt";

    public LigneTypeAdapter() {
    }

    @Override
    public Ligne read(final JsonReader in) throws IOException {

        Ligne ligne = new Ligne();
        Ligne.Type type;
        String[] split;
        String[] types = {"SEM","C38","CHRONO","FLEXO","TRAM","PROXIMO"};

        in.beginObject();
        while (in.hasNext()) {
            switch (in.nextName()) {
                case "id":
                    split = in.nextString().split(":");
                    Stop.Reseau reseau;
                    try {
                        reseau = Stop.Reseau.valueOf(split[0]);
                    }
                    catch (Exception e) {
                        reseau = Stop.Reseau.INCONU;
                    }
                    ligne.setReseau(reseau);
                    ligne.setId(split[1]);
                    break;
                case "textColor":
                    if(in.peek() != JsonToken.NULL)
                        ligne.setTextColor(in.nextString());
                    else
                        in.skipValue();
                    break;
                case "color":
                    if(in.peek() != JsonToken.NULL)
                        ligne.setColor(in.nextString());
                    else
                        in.skipValue();
                    break;
                case "type":
                    try {
                        type = Ligne.Type.valueOf(in.nextString());
                    }
                    catch (Exception e) {
                        type = Ligne.Type.INCONNU;
                    }
                    ligne.setType(type);
                    break;
                default:
                    in.skipValue();
                    break;
            }
        }
        in.endObject();
        if (ligne.getType() == Ligne.Type.INCONNU) {
            return null;
        }
        return ligne;
    }

    @Override
    public void write(JsonWriter out, Object value) throws IOException {
        //todo
    }
}
