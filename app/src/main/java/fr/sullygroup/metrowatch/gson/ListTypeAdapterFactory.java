package fr.sullygroup.metrowatch.gson;

import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.TypeAdapterFactory;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by jocelyn.caraman on 20/11/2017.
 */

public class ListTypeAdapterFactory implements TypeAdapterFactory {

    static public TypeAdapterFactory getListTypeAdapterFactory() {
        return new ListTypeAdapterFactory();
    }
    public <T> TypeAdapter<T> create(Gson gson, TypeToken<T> typeToken) {
        Type type = typeToken.getType();
        if (typeToken.getRawType() != List.class
                || !(type instanceof ParameterizedType)) {
            return null;
        }

        Type elementType = ((ParameterizedType) type).getActualTypeArguments()[0];
        TypeAdapter<?> elementAdapter = gson.getAdapter(TypeToken.get(elementType));
        return (TypeAdapter<T>) newListAdapter(elementAdapter);
    }

    private <E> TypeAdapter<List<E>> newListAdapter(final TypeAdapter<E> elementAdapter) {
        return new TypeAdapter<List<E>>() {
            public void write(JsonWriter out, List<E> list) throws IOException {
                if (list == null) {
                    out.nullValue();
                    return;
                }

                out.beginArray();
                for (E e : list) {
                    elementAdapter.write(out, e);
                }
                out.endArray();
            }

            public List<E> read(JsonReader in) throws IOException {
                if (in.peek() == JsonToken.NULL) {
                    in.nextNull();
                    return null;
                }

                List<E> result = new ArrayList<>();
                in.beginArray();
                while (in.hasNext()) {
                    E element = elementAdapter.read(in);
                    if(element != null)
                        result.add(element);
                }
                in.endArray();
                return result;
            }
        };
    }
}