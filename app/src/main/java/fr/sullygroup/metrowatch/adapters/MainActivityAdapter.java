package fr.sullygroup.metrowatch.adapters;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PagerSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.List;

import fr.sullygroup.metrowatch.R;
import fr.sullygroup.metrowatch.activities.MainActivity;
import fr.sullygroup.metrowatch.databinding.ItemRvMainactivityStopLigneBinding;
import fr.sullygroup.metrowatch.databinding.ItemRvMainactivityTitreLogosBinding;
import fr.sullygroup.metrowatch.model.Arret;
import fr.sullygroup.metrowatch.model.Ligne;
import fr.sullygroup.metrowatch.model.StopLigne;
import fr.sullygroup.metrowatch.network.Resource;

/**
 * Created by jocelyn.caraman on 29/11/2017.
 */

public class MainActivityAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private final static int VIEWTYPE_TITRE_LOGOS = 0;
    private final static int VIEWTYPE_ITEM_STOP_LIGNE_DIR1 = 1;
    private final static int VIEWTYPE_ITEM_STOP_LIGNE_DIR2 = 2;
    private final static int VIEWTYPE_SEPARATOR = 3;
    private final static int VIEWTYPE_LOADING = 4;
    private final static int VIEWTYPE_ERROR = 5;

    private Resource<List<StopLigne>> resourceDir1;
    private Resource<List<StopLigne>> resourceDir2;
    private List<Ligne> lignes;
    private Arret arret;

    private MainActivity activity;

    private class StopLigneViewHolder extends RecyclerView.ViewHolder {
        public int dir;
        StopLigneViewHolder(View v,int mDir) {
            super(v);
            dir = mDir;
        }
    }

    private class TitreLogosViewHolder extends RecyclerView.ViewHolder {
        public RecyclerView recyclerView;
        public LogoAdapter logoAdapter;
        public PagerSnapHelper snapHelper;

        TitreLogosViewHolder(View v) {
            super(v);
            recyclerView = v.findViewById(R.id.recycler_logos);
            logoAdapter = new LogoAdapter((MainActivity)recyclerView.getContext());
            snapHelper = new PagerSnapHelper() {
                @Nullable
                @Override
                public View findSnapView(RecyclerView.LayoutManager layoutManager) {
                View view = super.findSnapView(layoutManager);
                if( recyclerView.getContext() instanceof MainActivity)
                    ((MainActivity)recyclerView.getContext()).onSnap(view);
                return view;
                }
            };
            snapHelper.attachToRecyclerView(recyclerView);
            recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext(),LinearLayoutManager.HORIZONTAL,false));
            recyclerView.setOnFlingListener(snapHelper);
            recyclerView.setAdapter(logoAdapter);
            logoAdapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
                @Override
                public void onChanged() {
                    super.onChanged();
                    recyclerView.scrollToPosition(1);
                    recyclerView.fling(-1000,0);
                    logoAdapter.unregisterAdapterDataObserver(this);
                }
            });
        }

        public void onBindLignes(List<Ligne> lignes) {
            logoAdapter.setLignes(lignes);
        }

        public void onBindArret(Arret arret) {
            ViewDataBinding binding = DataBindingUtil.findBinding(itemView);
            if(binding != null && binding instanceof ItemRvMainactivityTitreLogosBinding) {
                ((ItemRvMainactivityTitreLogosBinding) binding).setArret(arret);
            }
        }
    }

    private class SeparatorViewHolder extends RecyclerView.ViewHolder {
        SeparatorViewHolder(View v) {
            super(v);
        }
    }

    private class DummyViewHolder extends RecyclerView.ViewHolder {
        DummyViewHolder(View v) {
            super(v);
        }
    }

    public MainActivityAdapter(MainActivity activity) {
        resourceDir1 = Resource.notSet(null);
        resourceDir2 = Resource.notSet(null);
        lignes = new ArrayList<>(0);
        arret = new Arret();
        arret.setNom("");
        this.activity = activity;
    }

    public void setLignes(List<Ligne> mLignes) {
        lignes = mLignes;
        notifyItemChanged(0);
    }

    public void setStopLignesDir1(Resource<List<StopLigne>> mResource) {
        Resource<List<StopLigne>> old = resourceDir1;
        if(mResource != null) {
            resourceDir1 = mResource;
            notifyAdapterStatusChange(old.status,mResource.status);
            notifyAdapterDir1(old,mResource);
        }
    }

    public void setStopLignesDir2(Resource<List<StopLigne>> mResource) {
        Resource<List<StopLigne>> old = resourceDir2;
        if(mResource != null) {
            resourceDir2 = mResource;
            notifyAdapterDir2(old,mResource);
        }
    }

    public void setArret(Arret mArret) {
        if(mArret != null) {
            arret = mArret;
            notifyItemChanged(0);
        }
    }

    private void notifyAdapterStatusChange(Resource.Status oldStatus, Resource.Status newStatus) {
        if(oldStatus == Resource.Status.NOTSET){
            switch (newStatus){
                case LOADING_ALL:
                    notifyItemInserted(1);
                    break;
                default:
                    Log.e("test","je ne suis pas géré 1"+ oldStatus +"->"+ newStatus);
            }
        }
        else if(oldStatus == Resource.Status.LOADING_ALL) {
            switch (newStatus){
                case SUCCESS:
                case LOADING_PARTIALLY:
                    notifyItemRemoved(1);
                    break;
                case ERROR:
                    notifyItemChanged(1);
                    break;
                default:
                    Log.e("test","je ne suis pas géré 2"+ oldStatus +"->"+ newStatus);
            }
        }
        else if(oldStatus == Resource.Status.LOADING_PARTIALLY) {
            switch (newStatus) {
                case ERROR:
                    notifyItemInserted(1);
                    break;
                default:
                    Log.e("test", "je ne suis pas géré 2bis" + oldStatus + "->" + newStatus);
            }
        }
        else if(oldStatus == Resource.Status.ERROR) {
            switch (newStatus) {
                case SUCCESS:
                case LOADING_PARTIALLY:
                    notifyItemRemoved(1);
                    break;
                case LOADING_ALL:
                    notifyItemChanged(1);
                    break;
                default:
                    Log.e("test","je ne suis pas géré 3"+ oldStatus +"->"+ newStatus);
            }
        }
        else if(oldStatus == Resource.Status.SUCCESS) {
            switch (newStatus) {
                case LOADING_ALL:
                    notifyItemInserted(1);
                    break;
                default:
                    Log.e("test","je ne suis pas géré 4" + oldStatus +"->"+ newStatus);
            }
        }
    }

    void notifyAdapterDir1(Resource<List<StopLigne>> old, Resource<List<StopLigne>> newR) {
        int oldSize = (old.data != null ? old.data.size() : 0);
        int newSize = (newR.data != null ? newR.data.size() : 0);
        int sizeDir2 = (resourceDir2.data != null ? resourceDir2.data.size() : 0);
        Resource.Status status = resourceDir1.status;
        Log.d("notify dir1","oldsize " + oldSize + " newsize " + newSize + " sizeDir2 " + sizeDir2 + " status " + status);

        if(oldSize == 0 && newSize > 0){
            int posPremierElementAInserer = 1;
            notifyItemRangeInserted(posPremierElementAInserer,newSize + (sizeDir2 > 0 ? 1 : 0)/*separator*/);
        }
        else if(oldSize < newSize && oldSize != 0){
            int posPremierItemDir1 = 1;
            int posDernierItemDir1 = posPremierItemDir1 + oldSize - 1;
            notifyItemRangeChanged(posPremierItemDir1,newSize - oldSize);
            notifyItemRangeInserted(posDernierItemDir1 + 1,newSize - oldSize);
        }
        else if(oldSize > newSize && newSize != 0) {
            int posPremierItemDir1 = 1;
            int posDernierItemDir1 = posPremierItemDir1 + oldSize - 1;
            notifyItemRangeChanged(posPremierItemDir1,oldSize - newSize);
            notifyItemRangeRemoved(posDernierItemDir1 - (oldSize - newSize) + 1,oldSize - newSize);
        }
        else if(oldSize > 0 && newSize == 0) {
            int posPremierItemDir1 = 1 + (status == Resource.Status.LOADING_ALL || status == Resource.Status.ERROR ? 1 : 0);
            int posDernierItemDir1 = posPremierItemDir1 + oldSize - 1;
            notifyItemRangeRemoved(posDernierItemDir1 - (oldSize - newSize) + 1,oldSize - newSize + (sizeDir2 >0?1:0));
        }
        else if(oldSize == newSize){
            if(oldSize != 0) {
                int posPremierItemDir1 = 1 + (status == Resource.Status.LOADING_ALL || status == Resource.Status.ERROR ? 1 : 0);
                notifyItemRangeChanged(posPremierItemDir1,newSize);
            }
        }
        else
            Log.d("notify", "cas non géré !!");
    }

    void notifyAdapterDir2(Resource<List<StopLigne>> old, Resource<List<StopLigne>> newR) {
        int oldSize = (old.data != null ? old.data.size() : 0);
        int newSize = (newR.data != null ? newR.data.size() : 0);
        int sizeDir1 = (resourceDir1.data != null ? resourceDir1.data.size() : 0);
        Resource.Status status = resourceDir1.status;
        Log.d("notify dir2","oldsize " + oldSize + " newsize " + newSize + " sizeDir1 " + sizeDir1+ " status " + status);

        if(oldSize == 0 && newSize > 0){
            int posPremierElementAInserer = 1 + sizeDir1;
            notifyItemRangeInserted(posPremierElementAInserer,(sizeDir1 > 0 ? 1 : 0)/*separator*/ + newSize);
        }
        else if(oldSize < newSize && oldSize != 0){
            int posPremierItemDir2 = (1 + sizeDir1 + (sizeDir1 > 0 ? 1 : 0) /*sepa*/);
            int posDernierItemDir2 = posPremierItemDir2 + oldSize - 1;
            notifyItemRangeChanged(posPremierItemDir2,newSize - oldSize);
            notifyItemRangeInserted(posDernierItemDir2,newSize - oldSize);
        }
        else if(oldSize > newSize && newSize != 0) {
            int posPremierItemDir2 = (1 + sizeDir1 + (sizeDir1 > 0 ? 1 : 0) /*sepa*/);
            int posDernierItemDir2 = posPremierItemDir2 + oldSize - 1;
            notifyItemRangeChanged(posPremierItemDir2,oldSize - newSize);
            notifyItemRangeRemoved(posDernierItemDir2 - (oldSize - newSize) + 1,oldSize - newSize);
        }
        else if(oldSize > 0 && newSize == 0) {
            int posPremierItemDir2 = (1 + (status == Resource.Status.LOADING_ALL || status == Resource.Status.ERROR ? 1 : 0) + sizeDir1 + (sizeDir1 > 0 ? 1 : 0) /*sepa*/);
            int posDernierItemDir2 = posPremierItemDir2 + oldSize - 1;
            notifyItemRangeRemoved(posDernierItemDir2 - (oldSize - newSize) - (sizeDir1 >0?1:0)+ 1,oldSize - newSize + (sizeDir1 >0?1:0));
        }
        else if(oldSize == newSize){
            if(oldSize != 0) {
                int posPremierItemDir2 = 1 +(status == Resource.Status.LOADING_ALL || status == Resource.Status.ERROR ? 1 : 0) + sizeDir1 + (sizeDir1 > 0 ? 1 : 0);
                notifyItemRangeChanged(posPremierItemDir2,newSize);
            }
        }
        else
            Log.d("notify", "cas non géré !!");
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        ViewDataBinding binding;
        switch (viewType){
            case VIEWTYPE_TITRE_LOGOS:
                //get arret from databinding
                binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_rv_mainactivity_titre_logos, parent, false);
                return new TitreLogosViewHolder(binding.getRoot());
            case VIEWTYPE_ITEM_STOP_LIGNE_DIR1:
                binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_rv_mainactivity_stop_ligne, parent, false);
                setupAnimation(binding.getRoot());
                return new StopLigneViewHolder(binding.getRoot(),1);
            case VIEWTYPE_ITEM_STOP_LIGNE_DIR2:
                binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_rv_mainactivity_stop_ligne, parent, false);
                setupAnimation(binding.getRoot());
                return new StopLigneViewHolder(binding.getRoot(),2);
            case VIEWTYPE_SEPARATOR:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_rv_mainactivity_separator,parent,false);
                return new SeparatorViewHolder(view);
            case VIEWTYPE_ERROR:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_rv_mainactivity_error,parent,false);
                return new DummyViewHolder(view);
            case VIEWTYPE_LOADING:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_rv_mainactivity_loading,parent,false);
                ImageView loading = view.findViewById(R.id.loading);
                Drawable drawable = loading.getDrawable();
                if (drawable instanceof Animatable) {
                    ((Animatable) drawable).start();
                }
                return new DummyViewHolder(view);
            default:
                return null;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(holder instanceof StopLigneViewHolder) {
            ViewDataBinding binding = DataBindingUtil.findBinding(holder.itemView);
            if(binding != null && binding instanceof ItemRvMainactivityStopLigneBinding) {
                int sizeDir1 = (resourceDir1.data != null ? resourceDir1.data.size() : 0);
                int sizeDir2 = (resourceDir2.data != null ? resourceDir2.data.size() : 0);
                int dir = ((StopLigneViewHolder)holder).dir;
                if(dir == 1) {
                    if(resourceDir1.data != null) {
                        ((ItemRvMainactivityStopLigneBinding)binding).setStopLigne(resourceDir1.data.get(position - 1));
                        ((ItemRvMainactivityStopLigneBinding)binding).setResource(resourceDir1);
                    }

                }
                else {
                    if(resourceDir2.data != null) {
                        ((ItemRvMainactivityStopLigneBinding)binding).setResource(resourceDir2);
                        ((ItemRvMainactivityStopLigneBinding)binding).setStopLigne(resourceDir2.data.get(position - 1 + ((sizeDir1 == 0) ? 0 : - 1 - sizeDir1)));
                    }
                }
            }
        }
        else if (holder instanceof TitreLogosViewHolder) {
            TitreLogosViewHolder tlViewHolder = (TitreLogosViewHolder)holder;
            tlViewHolder.onBindArret(arret);
            tlViewHolder.onBindLignes(lignes);
        }
        else if (holder instanceof SeparatorViewHolder) {
            // rien pour l'instant
        }
    }

    @Override
    public int getItemCount() {
        int sizeDir1 = (resourceDir1.data != null ? resourceDir1.data.size() : 0);
        int sizeDir2 = (resourceDir2.data != null ? resourceDir2.data.size() : 0);
        Resource.Status status = resourceDir1.status != Resource.Status.SUCCESS ? resourceDir1.status : Resource.Status.NOTSET;

        return 1 + sizeDir1 + (sizeDir1 == 0 || sizeDir2 == 0 ? 0 : 1)
                + sizeDir2 + (status == Resource.Status.LOADING_ALL || status == Resource.Status.ERROR ? 1 : 0);
    }

    @Override
    public int getItemViewType(int position) {
        switch(position) {
            case 0: return VIEWTYPE_TITRE_LOGOS;
            default:
                int sizeDir1 = (resourceDir1.data != null ? resourceDir1.data.size() : 0);
                int sizeDir2 = (resourceDir2.data != null ? resourceDir2.data.size() : 0);
                Resource.Status status = resourceDir1.status;

                if(status == Resource.Status.NOTSET) {
                    Log.e("maadap","No status");
                    return super.getItemViewType(position);
                }
                if(status == Resource.Status.LOADING_ALL)
                    return VIEWTYPE_LOADING;
                if(status == Resource.Status.ERROR)
                    return VIEWTYPE_ERROR;

                if(sizeDir1 == 0 && sizeDir2 == 0)
                {
                    return super.getItemViewType(position);
                }
                else if(sizeDir1 > 0 && sizeDir2 == 0) {
                    if(1 <= position && position <= sizeDir1) {
                        return VIEWTYPE_ITEM_STOP_LIGNE_DIR1;
                    }
                }
                else if(sizeDir2 > 0 && sizeDir1 == 0) {
                    if(1 <= position && position <= sizeDir2) {
                        return VIEWTYPE_ITEM_STOP_LIGNE_DIR2;
                    }
                }
                else if(sizeDir1 > 0 && sizeDir2 > 0) {
                    if( 1 <= position && position <= sizeDir1)
                        return VIEWTYPE_ITEM_STOP_LIGNE_DIR1;
                    else if(sizeDir1 + 1 + 1 <= position && position <= sizeDir1 + 1 + sizeDir2)
                        return VIEWTYPE_ITEM_STOP_LIGNE_DIR2;
                    else if(position == sizeDir1 + 1)
                        return VIEWTYPE_SEPARATOR;
                }
                return super.getItemViewType(position);
        }
    }

    private void setupAnimation(View view ) {
        final ImageView imageView = view.findViewById(R.id.loading);
        imageView.getViewTreeObserver().addOnGlobalLayoutListener(() -> {
            if(imageView.getVisibility() == View.VISIBLE) {
                Drawable drawable = imageView.getDrawable();
                if (drawable instanceof Animatable) {
                    ((Animatable) drawable).start();
                }
            }
        });
    }
}
