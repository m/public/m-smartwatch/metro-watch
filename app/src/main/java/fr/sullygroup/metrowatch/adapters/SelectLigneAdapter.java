package fr.sullygroup.metrowatch.adapters;

/**
 * Created by jocelyn.caraman on 31/10/2017.
 */

import android.app.Activity;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import fr.sullygroup.metrowatch.databinding.Presenter;
import fr.sullygroup.metrowatch.R;
import fr.sullygroup.metrowatch.activities.MainActivity;
import fr.sullygroup.metrowatch.databinding.ListItemLigneBinding;
import fr.sullygroup.metrowatch.model.Ligne;

public class SelectLigneAdapter extends RecyclerView.Adapter {
    private static final String TAG = "SelectLigneAdapter";
    private List<Ligne> mDataset;
    private MainActivity activity;

    private class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        ViewHolder(View v) {
            super(v);
            itemView.setOnClickListener(this);
        }
        @Override
        public void onClick(View view) {
            //activity.handleItemClick(getAdapterPosition());
        }
    }

    public SelectLigneAdapter(Activity mActivity, List<Ligne> myDataset) {
        mDataset = myDataset;
        if(mActivity instanceof MainActivity)
            activity = (MainActivity) mActivity;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ListItemLigneBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.list_item_ligne, parent, false);
        return new ViewHolder(binding.getRoot());
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ViewDataBinding binding = DataBindingUtil.findBinding(holder.itemView);
        if(binding != null && binding instanceof ListItemLigneBinding) {
            ((ListItemLigneBinding)binding).setLigne(mDataset.get(position));
            ((ListItemLigneBinding)binding).setPresenter(new Presenter(activity));
        }
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}
