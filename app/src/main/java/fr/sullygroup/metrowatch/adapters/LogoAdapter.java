package fr.sullygroup.metrowatch.adapters;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Space;

import java.util.ArrayList;
import java.util.List;

import fr.sullygroup.metrowatch.databinding.Presenter;
import fr.sullygroup.metrowatch.R;
import fr.sullygroup.metrowatch.activities.MainActivity;
import fr.sullygroup.metrowatch.databinding.LogoBinding;
import fr.sullygroup.metrowatch.databinding.LogoRectangleBinding;
import fr.sullygroup.metrowatch.model.Ligne;

/**
 * Created by jocelyn.caraman on 06/11/2017.
 */

public class LogoAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private static final int VIEWTYPE_AUTRES = 0;
    private static final int VIEWTYPE_RECTANGLE = 1;
    private static final int VIEWTYPE_DUMMY = 2;
    private List<Ligne> mDataset;
    private MainActivity activity;

    private class LogoViewHolder extends RecyclerView.ViewHolder {
        LogoViewHolder(View v) {
            super(v);
        }
    }

    private class DummyViewHolder extends RecyclerView.ViewHolder {
        DummyViewHolder(View v) {
            super(v);
        }
    }

    public LogoAdapter(MainActivity mActivity) {
        this.activity = mActivity;
        this.mDataset = new ArrayList<>();
    }

    public void setLignes(List<Ligne> dataset) {
        this.mDataset = dataset;
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ViewDataBinding binding = null;
        switch (viewType){
            case VIEWTYPE_AUTRES:
                binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.logo, parent, false);
                break;
            case VIEWTYPE_RECTANGLE:
                binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.logo_rectangle, parent, false);
                break;
            case VIEWTYPE_DUMMY:
                Space space = new Space(activity);
                space.setBackgroundColor(Color.CYAN);
                int widthDP = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 45, activity.getResources().getDisplayMetrics());
                RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(widthDP, ViewGroup.LayoutParams.MATCH_PARENT);
                space.setLayoutParams(params);
                return new DummyViewHolder(space);
        }
        return new LogoAdapter.LogoViewHolder(binding.getRoot());
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ViewDataBinding binding = DataBindingUtil.findBinding(holder.itemView);
        if(binding != null) {
            final Ligne ligne = mDataset.get(position-1);
            if (binding instanceof LogoRectangleBinding) {
                ((LogoRectangleBinding) binding).setLigne(ligne);
                ((LogoRectangleBinding) binding).setPresenter(new Presenter(activity));
            }
            if (binding instanceof LogoBinding) {
                ((LogoBinding) binding).setLigne(ligne);
                ((LogoBinding) binding).setPresenter(new Presenter(activity));
            }
            binding.getRoot().setOnClickListener(v -> {
                if (activity != null)
                    activity.afficherLigne(ligne);
            });
            binding.getRoot().setOnLongClickListener(v -> {
                if (activity != null)
                    activity.showDialog(mDataset);
                return true;
            });
        }
    }

    @Override
    public int getItemViewType(int position) {
        if(position == 0 || position == mDataset.size() + 1)
            return VIEWTYPE_DUMMY;
        Ligne l = mDataset.get(position-1);
        if(l.getType() == Ligne.Type.C38) {
            return VIEWTYPE_RECTANGLE;
        }
        return VIEWTYPE_AUTRES;
    }

    @Override
    public int getItemCount() {
        return mDataset.size()+2;
    }
}
