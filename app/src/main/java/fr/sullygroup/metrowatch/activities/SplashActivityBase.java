package fr.sullygroup.metrowatch.activities;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ImageView;

import java.util.List;

import javax.inject.Inject;

import dagger.android.AndroidInjection;
import fr.sullygroup.metrowatch.BuildConfig;
import fr.sullygroup.metrowatch.Constantes;
import fr.sullygroup.metrowatch.R;
import fr.sullygroup.metrowatch.Utils;
import fr.sullygroup.metrowatch.model.Ligne;
import fr.sullygroup.metrowatch.model.Stop;
import fr.sullygroup.metrowatch.network.retrofit.WebService;
import io.objectbox.BoxStore;
import io.objectbox.android.AndroidObjectBrowser;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by jocelyn.caraman on 09/10/2017.
 */

public abstract class SplashActivityBase extends AppCompatActivity {
    public static final String TAG = "SplashActivity";

    protected final static int DATA_INFO_LIGNE = 0;
    protected final static int DATA_STOPS = 1;

    protected AppCompatActivity _this = this;

    protected boolean isDataReady = false;
    protected boolean isDataInfoLigneDownloaded = false;
    protected boolean isDataStopsDownloaded = false;
    protected double lat = Constantes.DEFAULT_LATITUDE;
    protected double longi = Constantes.DEFAULT_LONGITUDE;

    @Inject
    protected WebService service;

    @Inject
    protected BoxStore boxStore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d("test", ((this instanceof SplashActivity)+""));
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splashscreen);
        if (BuildConfig.DEBUG) {
            new AndroidObjectBrowser(boxStore).start(this);
        }
        ImageView logo = findViewById(R.id.logo);
        Drawable drawable = logo.getDrawable();
        if (drawable instanceof Animatable) {
            ((Animatable) drawable).start();
        }
        if (!hasGps()) {
            Log.d(TAG, "This hardware doesn't have GPS.");
        }

        Utils.clearAllStores(boxStore);
    }


    protected boolean hasGps() {
        return getPackageManager().hasSystemFeature(PackageManager.FEATURE_LOCATION_GPS);
    }

    protected void startMainActivity(){
        Intent intent = new Intent(_this,MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    protected void setDataReady(int which, boolean dataReady) {
        switch (which) {
            case DATA_INFO_LIGNE : isDataInfoLigneDownloaded = dataReady;
                break;
            case DATA_STOPS : isDataStopsDownloaded = dataReady;
                break;
        }
        isDataReady = isDataStopsDownloaded && isDataInfoLigneDownloaded;
        if(canLaunchMainActivity())
            startMainActivity();
    }

    protected abstract boolean canLaunchMainActivity();

    protected void getData() {
        service.getInfoLignes().enqueue(new Callback<List<Ligne>>() {
            @Override
            public void onResponse(@NonNull Call<List<Ligne>> call, @NonNull Response<List<Ligne>> response) {
                Log.d("retro",response.toString());
                List<Ligne> lignes = response.body();
                if(lignes != null && lignes.size() > 0) {
                    boxStore.boxFor(Ligne.class).put(lignes);

                    service.getLinesNear(String.valueOf(longi),String.valueOf(lat)).enqueue(new Callback<List<Stop>>() {
                        @Override
                        public void onResponse(@NonNull Call<List<Stop>> call, @NonNull Response<List<Stop>> response) {
                            Log.d("retro",response.toString());
                            if(Utils.saveStopsFromWebservices(boxStore, response, TAG))
                                setDataReady(DATA_STOPS,true);
                        }

                        @Override
                        public void onFailure(@NonNull Call<List<Stop>> call, @NonNull Throwable t) {
                            Log.e("Retrofit","Error getting lines near : " + t.getMessage());
                            t.printStackTrace();
                        }

                    });
                }
                else
                    Log.e(TAG,"Prb obtention lignes !");
                setDataReady(DATA_INFO_LIGNE,true);
            }

            @Override
            public void onFailure(@NonNull Call<List<Ligne>> call, @NonNull Throwable t) {
                Log.e("Retrofit","Error getting lines info : " + t.getMessage());
                t.printStackTrace();
                try {
                    Thread.sleep(1000);
                    getData();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

        });
    }
}
