package fr.sullygroup.metrowatch.activities;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.wear.widget.WearableRecyclerView;
import android.util.Log;
import android.util.SparseArray;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import javax.inject.Inject;

import dagger.android.AndroidInjection;
import fr.sullygroup.metrowatch.R;
import fr.sullygroup.metrowatch.fragments.SelectLigneDialog;
import fr.sullygroup.metrowatch.adapters.MainActivityAdapter;
import fr.sullygroup.metrowatch.databinding.LogoBinding;
import fr.sullygroup.metrowatch.databinding.LogoRectangleBinding;
import fr.sullygroup.metrowatch.interfaces.SnapListener;
import fr.sullygroup.metrowatch.model.Ligne;
import fr.sullygroup.metrowatch.model.Passage;
import fr.sullygroup.metrowatch.model.StopLigne;
import fr.sullygroup.metrowatch.network.Resource;
import fr.sullygroup.metrowatch.viewmodel.ArretViewModel;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

public class MainActivity extends AppCompatActivity implements SnapListener {

    public static final String TAG = "MainActivity";

    private long idLastLigne = -1;

    private GestureDetectorCompat mDetector;

    private MainActivityAdapter mainActivityAdapter;
    private WearableRecyclerView wearableRecyclerView;
    private LinearLayout emptyLayout;
    private ArretViewModel viewModel;

    private Observer<Resource<List<StopLigne>>> horairesObserver = resource -> {
        SparseArray<Resource<List<StopLigne>>> sparseArray = trierPassage(resource);
        if(sparseArray != null) {
            mainActivityAdapter.setStopLignesDir1(sparseArray.get(1));
            mainActivityAdapter.setStopLignesDir2(sparseArray.get(2));
        }

        Log.d("testRes",resource.status + " " + (resource.data != null));
    };

    Comparator<Passage> passageComparator = Comparator.comparingLong(p -> p.horaireEnSeconde);

    private SparseArray<Resource<List<StopLigne>>> trierPassage(Resource<List<StopLigne>> resource) {
        if(resource.data != null){
            SparseArray<Resource<List<StopLigne>>> stopLigneParDirection = new SparseArray<>(2);
            List<Passage> passagesDir1 = new ArrayList<>();
            List<Passage> passagesDir2 = new ArrayList<>();
            List<StopLigne> stops = resource.data;
            for (StopLigne stop : stops) {
                if(stop.getCodeDirection() == 1) {
                    passagesDir1.addAll(stop.getPassages());

                } else {
                    passagesDir2.addAll(stop.getPassages());

                }
            }

            if(passagesDir1.isEmpty() && passagesDir2.isEmpty()) {
                // Cas ou Passages vide car refresh
                List<StopLigne> stopLignesDir1 = new ArrayList<>();
                List<StopLigne> stopLignesDir2 = new ArrayList<>();
                for (StopLigne stop : stops) {
                    if(stop.getCodeDirection() == 1) {
                        stopLignesDir1.add(stop);
                    } else {
                        stopLignesDir2.add(stop);
                    }
                }

                stopLigneParDirection.append(1, Resource.fromStatus(resource.status,stopLignesDir1));
                stopLigneParDirection.append(2, Resource.fromStatus(resource.status,stopLignesDir2));
            }
            else {
                passagesDir1.sort(passageComparator);
                passagesDir2.sort(passageComparator);

                stopLigneParDirection.append(1, Resource.fromStatus(resource.status,passagesSortedToStopLignesSorted(passagesDir1)));
                stopLigneParDirection.append(2, Resource.fromStatus(resource.status,passagesSortedToStopLignesSorted(passagesDir2)));
            }
            return stopLigneParDirection;
        }
        else {
            //todo
        }
        return null;
    }

    private List<StopLigne> passagesSortedToStopLignesSorted(List<Passage> passages) {
        List<StopLigne> stopLignes = new ArrayList<>();
        for(Passage p : passages) {
            StopLigne sl = p.stopLigne.getTarget();
            if(sl != null && !stopLignes.contains(sl))
                stopLignes.add(sl);
        }
        return stopLignes;
    }

    @Inject
    ViewModelProvider.Factory viewModelFactory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        viewModel = ViewModelProviders.of(this,viewModelFactory).get(ArretViewModel.class);
        viewModel.init();

        Bundle bundle = getIntent().getExtras();
        if(bundle != null)
        {
            double lat = bundle.getDouble("lat");
            double lon = bundle.getDouble("lon");
        }

        setupViews();

        viewModel.arrets.observe(this, arrets -> {
            showViews(arrets.isEmpty());
        });

        viewModel.arret.observe(this, arret -> {
            if(arret != null) {
                mainActivityAdapter.setArret(arret);
                mainActivityAdapter.setLignes(arret.getLignes());
            }
        });

        viewModel.horaires.observe(this,horairesObserver);

        mDetector = new GestureDetectorCompat(this, new MyGestureListener());
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        //Log.d("debug", "dispatchTouchEvent");
        //this.mDetector.onTouchEvent(event);
        return  super.dispatchTouchEvent(event);
    }

    private void setupViews() {
        mainActivityAdapter = new MainActivityAdapter(this);
        wearableRecyclerView = findViewById(R.id.main_recycler);
        wearableRecyclerView.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));
        wearableRecyclerView.setAdapter(mainActivityAdapter);
        emptyLayout = findViewById(R.id.empty_layout);
        Button retryButton = findViewById(R.id.retry_button);
        retryButton.setOnClickListener(v -> {
            triggerRebirth(this);
        });
    }

    private void showViews(boolean emptyStops) {
        if(emptyStops) {
            wearableRecyclerView.setVisibility(View.GONE);
            emptyLayout.setVisibility(View.VISIBLE);
        } else {
            wearableRecyclerView.setVisibility(View.VISIBLE);
            emptyLayout.setVisibility(View.GONE);
        }
    }

    @Override
    public void onSnap(View view) {
        if(view != null) {
            ViewDataBinding binding = DataBindingUtil.findBinding(view);
           if(binding != null)
            {
                Ligne ligne = null;
                if (binding instanceof LogoRectangleBinding) {
                    ligne = ((LogoRectangleBinding) binding).getLigne();
                }
                else if(binding instanceof LogoBinding){
                    ligne = ((LogoBinding) binding).getLigne();
                }
                if(ligne != null) afficherLigne(ligne);
            }
        }
    }

    public void afficherLigne(Ligne mLigne) {
        if(mLigne.getNumId() != idLastLigne) {
            ((MutableLiveData<Ligne>)viewModel.ligne).setValue(mLigne);
            idLastLigne = mLigne.getNumId();
        }
    }

    public void showDialog(List<Ligne> list) {

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment prev = getSupportFragmentManager().findFragmentByTag("dialog");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        // Create and show the dialog.
        DialogFragment newFragment = SelectLigneDialog.newInstance(new ArrayList<>(list));
        newFragment.show(ft, "dialog");
    }

    public void dismissDiallog() {
        Fragment prev = getSupportFragmentManager().findFragmentByTag("dialog");
        if (prev != null) {
            ((DialogFragment)prev).dismiss();
        }
    }

    public static void triggerRebirth(Context context) {
        PackageManager pm = context.getPackageManager();
        Intent intent = pm.getLaunchIntentForPackage(
                context.getPackageName()
        );
        intent.addFlags(FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
        if (context instanceof AppCompatActivity) {
            ((AppCompatActivity) context).finish();
        }

        Runtime.getRuntime().exit(0);
    }

    public static void doRestart(Context c) {
        try {
            //check if the context is given
            if (c != null) {
                //fetch the packagemanager so we can get the default launch activity
                // (you can replace this intent with any other activity if you want
                PackageManager pm = c.getPackageManager();
                //check if we got the PackageManager
                if (pm != null) {
                    //create the intent with the default start activity for your application
                    Intent mStartActivity = pm.getLaunchIntentForPackage(
                            c.getPackageName()
                    );
                    if (mStartActivity != null) {
                        mStartActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        //create a pending intent so the application is restarted after System.exit(0) was called.
                        // We use an AlarmManager to call this intent in 100ms
                        int mPendingIntentId = 223344;
                        PendingIntent mPendingIntent = PendingIntent
                                .getActivity(c, mPendingIntentId, mStartActivity,
                                        PendingIntent.FLAG_CANCEL_CURRENT);
                        AlarmManager mgr = (AlarmManager) c.getSystemService(Context.ALARM_SERVICE);
                        mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 1, mPendingIntent);
                        //kill the application
                        System.exit(0);
                    } else {
                        Log.e(TAG, "Was not able to restart application, mStartActivity null");
                    }
                } else {
                    Log.e(TAG, "Was not able to restart application, PM null");
                }
            } else {
                Log.e(TAG, "Was not able to restart application, Context null");
            }
        } catch (Exception ex) {
            Log.e(TAG, "Was not able to restart application");
        }
    }

    class MyGestureListener extends GestureDetector.SimpleOnGestureListener {
        private static final String DEBUG_TAG = "Gestures";
        private float downEventX = -1;
        private float downEventY = -1;
        private boolean isSwipeHandled = false;

        @Override
        public boolean onDown(MotionEvent event) {
            //Log.d(DEBUG_TAG,"onDown: " + event.toString());
            return true;
        }

        @Override
        public boolean onScroll(MotionEvent event1, MotionEvent event2,
                                float velocityX, float velocityY) {
            //Log.d(DEBUG_TAG, "onScroll: " + event1.toString() + event2.toString());
            //Log.d(DEBUG_TAG, "onScroll: " + velocityX + " " +velocityY);
            /*if(downEventX == -1 || downEventY == -1 || !equalsMotionEvent(event1)) {
                Log.d(DEBUG_TAG,"new event");
                downEventX = event1.getX();
                downEventY = event1.getY();
                isSwipeHandled = false;
            }

            float x1 = event1.getX();
            float y1 = event1.getY();
            float x2 = event2.getX();
            float y2 = event2.getY();

            float distY = y2 - y1;
            if(!isSwipeHandled){
                if(Math.abs(x2 - x1) < 40 && Math.abs(distY) > 100) {
                    if(distY > 0) {
                        Log.d(DEBUG_TAG,"defilement vers le bas");
                        if(positionLigne - 1 >= 0)
                            afficherLigne(DEPLACEMENT_PRECEDENT);
                    }

                    else {
                        Log.d(DEBUG_TAG,"defilement vers le haut");
                        if(positionLigne + 1 < arret.getLignes().size())
                            afficherLigne(DEPLACEMENT_SUIVANT);
                    }

                    isSwipeHandled = true;
                }
            }*/
            return true;
        }

        private boolean equalsMotionEvent(MotionEvent ev) {
            return ev.getX() == downEventX && ev.getY() == downEventY;
        }
    }

    private static int DEPLACEMENT_PRECEDENT = -1;
    private static int DEPLACEMENT_AUCUN = 0;
    private static int DEPLACEMENT_SUIVANT = 1;
}
