package fr.sullygroup.metrowatch;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.Nullable;
import android.support.wear.widget.WearableRecyclerView;
import android.util.AttributeSet;
import android.view.View;

import fr.sullygroup.metrowatch.network.Resource;

/**
 * Created by jocelyn.caraman on 22/11/2017.
 */

public class MyRecyclerView extends WearableRecyclerView {
    private View mEmptyView;
    private View loadingView;
    private int idEmptyView;
    private int idLoadingView;
    public MyRecyclerView(Context context) {
        this(context,null,0);
    }
    public MyRecyclerView(Context context, @Nullable AttributeSet attrs) {
        this(context,attrs,0);

    }
    public MyRecyclerView(Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        if(attrs != null) {
            TypedArray a = context.getTheme().obtainStyledAttributes(
                    attrs,
                    R.styleable.MyRecyclerView,
                    0, 0);

            try {
                idEmptyView = a.getResourceId(R.styleable.MyRecyclerView_emptyView,0);
                idLoadingView = a.getResourceId(R.styleable.MyRecyclerView_loadingView,0);
            } finally {
                a.recycle();
            }
        }
    }
    private void initViews() {
        if(mEmptyView != null && loadingView != null) {
            /*if(getAdapter() != null && getAdapter() instanceof StopListAdapter) {
                StopListAdapter ha = (StopListAdapter) getAdapter();
                Resource.Status status = ha.getStatus();
                if(status != null) {
                    mEmptyView.setVisibility(status == Resource.Status.ERROR ? VISIBLE : GONE);
                    loadingView.setVisibility(status == Resource.Status.LOADING ? VISIBLE : GONE);
                    MyRecyclerView.this.setVisibility(status == Resource.Status.SUCCESS ? VISIBLE : GONE);
                }
                else {
                    mEmptyView.setVisibility(VISIBLE);
                    loadingView.setVisibility(GONE);
                    MyRecyclerView.this.setVisibility(GONE);
                }
            }
            else {
                mEmptyView.setVisibility(VISIBLE);
                loadingView.setVisibility(GONE);
                MyRecyclerView.this.setVisibility(GONE);
            }*/
        }
    }

    final AdapterDataObserver observer = new AdapterDataObserver() {
        @Override
        public void onChanged() {
            super.onChanged();
            initViews();
        }
        @Override
        public void onItemRangeInserted(int positionStart, int itemCount) {
            super.onItemRangeInserted(positionStart, itemCount);
            initViews();
        }
        @Override
        public void onItemRangeRemoved(int positionStart, int itemCount) {
            super.onItemRangeRemoved(positionStart, itemCount);
            initViews();
        }
    };
    @Override
    public void setAdapter(Adapter adapter) {
        Adapter oldAdapter = getAdapter();
        super.setAdapter(adapter);
        if (oldAdapter != null) {
            oldAdapter.unregisterAdapterDataObserver(observer);
        }
        if (adapter != null) {
            adapter.registerAdapterDataObserver(observer);
        }
    }

    public void setEmptyView(View view) {
        this.mEmptyView = view;
        initViews();
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (idEmptyView != 0)
            mEmptyView = getRootView().findViewById(idEmptyView);
        if (idLoadingView != 0)
            loadingView = getRootView().findViewById(idLoadingView);
    }
}
