package fr.sullygroup.metrowatch.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.RecyclerView;
import android.support.wear.widget.CurvingLayoutCallback;
import android.support.wear.widget.WearableLinearLayoutManager;
import android.support.wear.widget.WearableRecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import fr.sullygroup.metrowatch.R;
import fr.sullygroup.metrowatch.adapters.SelectLigneAdapter;
import fr.sullygroup.metrowatch.model.Ligne;

/**
 * Created by jocelyn.caraman on 31/10/2017.
 */

public class SelectLigneDialog extends DialogFragment {
    private WearableRecyclerView mRecyclerView;
    private ArrayList<Ligne> list;
    /**
     * Create a new instance of MyDialogFragment, providing "num"
     * as an argument.
     */
    public static SelectLigneDialog newInstance(ArrayList<Ligne> mList) {
        SelectLigneDialog f = new SelectLigneDialog();
        Bundle args = new Bundle();
        args.putParcelableArrayList("lignes",mList);
        f.setArguments(args);

        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        list = getArguments().getParcelableArrayList("lignes");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialog_select_ligne, container, false);
        mRecyclerView = v.findViewById(R.id.recycler);
        mRecyclerView.setEdgeItemsCenteringEnabled(true);
        CurvingLayoutCallback mChildLayoutManager = new MyLauncherChildLayoutManager(getContext());
        mRecyclerView.setLayoutManager(new WearableLinearLayoutManager(getContext(),mChildLayoutManager));
        mRecyclerView.setAdapter(new SelectLigneAdapter(getActivity(),list));
        return v;
    }

    private class MyLauncherChildLayoutManager extends CurvingLayoutCallback {
        /** How much should we scale the icon at most. */
        private static final float MAX_ICON_PROGRESS = 0.65f;

        private float mProgressToCenter;

        MyLauncherChildLayoutManager(Context context) {
            super(context);
        }

        @Override
        public void onLayoutFinished(View child, RecyclerView parent) {
            super.onLayoutFinished(child, parent);

            // Figure out % progress from top to bottom
            float centerOffset = ((float) child.getHeight() / 2.0f) / (float) parent.getHeight();
            float yRelativeToCenterOffset = (child.getY() / parent.getHeight()) + centerOffset;

            // Normalize for center
            mProgressToCenter = Math.abs(0.5f - yRelativeToCenterOffset);
            // Adjust to the maximum scale
            mProgressToCenter = Math.min(mProgressToCenter, MAX_ICON_PROGRESS);

            child.setScaleX(1 - mProgressToCenter);
            child.setScaleY(1 - mProgressToCenter);
        }
    }
}
