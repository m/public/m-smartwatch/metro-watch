package fr.sullygroup.metrowatch.network;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/**
 * A generic class that describes a data with a status
 * Created by jocelyn.caraman on 14/11/2017.
 */

//
public class Resource<T> {
    @NonNull
    public final Status status;
    @Nullable
    public final T data;
    @Nullable public final String message;
    private Resource(@NonNull Status status, @Nullable T data, @Nullable String message) {
        this.status = status;
        this.data = data;
        this.message = message;
    }

    public static <T> Resource<T> success(@NonNull T data) {
        return new Resource<>(Status.SUCCESS, data, null);
    }

    public static <T> Resource<T> error(String msg, @Nullable T data) {
        return new Resource<>(Status.ERROR, data, msg);
    }

    public static <T> Resource<T> loadingPartially(@Nullable T data) {
        return new Resource<>(Status.LOADING_PARTIALLY, data, null);
    }

    public static <T> Resource<T> loadingAll(@Nullable T data) {
        return new Resource<>(Status.LOADING_ALL, data, null);
    }

    public static <T> Resource<T> notSet(@Nullable T data) {
        return new Resource<>(Status.NOTSET, data, null);
    }

    public static <T> Resource<T> fromStatus(Status status,@Nullable T data) {
        switch (status) {
            case SUCCESS:
                return success(data);
            case ERROR:
                return error("",data);
            case LOADING_ALL:
                return loadingAll(data);
            case LOADING_PARTIALLY:
                return loadingPartially(data);
            default:
                return null;
        }
    }
    public enum Status {
        LOADING_ALL,LOADING_PARTIALLY,ERROR,SUCCESS,NOTSET
    }

}