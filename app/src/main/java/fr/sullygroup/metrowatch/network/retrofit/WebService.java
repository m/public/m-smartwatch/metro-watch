package fr.sullygroup.metrowatch.network.retrofit;

import android.arch.lifecycle.LiveData;

import java.util.List;

import fr.sullygroup.metrowatch.model.StopLigne;
import fr.sullygroup.metrowatch.model.Ligne;
import fr.sullygroup.metrowatch.model.Stop;
import fr.sullygroup.metrowatch.network.ApiResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by jocelyn.caraman on 14/11/2017.
 */

public interface WebService {
    /**
     * @GET declares an HTTP GET request
     * @Path("user") annotation on the userId parameter marks it as a
     * replacement for the {user} placeholder in the @GET path
     */
    @GET("api/linesNear/json?dist=400&details=true")
    Call<List<Stop>> getLinesNear(@Query("x") String longitude, @Query("y") String latitude);

    @GET("api/routers/default/index/stops/{reseau}:{id}/stoptimes")
    LiveData<ApiResponse<List<StopLigne>>> getHoraires(@Path("id") String idStop, @Path("reseau") String reseau);

    @GET("api/routers/default/index/routes")
    Call<List<Ligne>> getInfoLignes();
}
