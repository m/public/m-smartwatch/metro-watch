package fr.sullygroup.metrowatch.network;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;
import android.os.AsyncTask;
import android.support.annotation.IntDef;
import android.support.annotation.MainThread;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.WorkerThread;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.Collection;
import java.util.List;

/**
 * ResultType: Type for the Resource data
 * RequestType: Type for the API response
 * Created by jocelyn.caraman on 14/11/2017.
 */


public abstract class NetworkBoundResource<RequestType, ResultType> {

    public static final int SHOULD_NOT_FETCH = 0;
    public static final int SHOULD_FETCH_ALL = 1;
    public static final int SHOULD_FETCH_PASSAGES = 2;

    @IntDef({
        SHOULD_NOT_FETCH,SHOULD_FETCH_ALL,SHOULD_FETCH_PASSAGES
    })
    @Retention(RetentionPolicy.SOURCE)
    public @interface ShouldFetch { }

    // Called to save the result of the API response into the database
    @WorkerThread
    protected abstract void saveCallResult(@NonNull RequestType item);

    // Called with the data in the database to decide whether it should be
    // fetched from the network.
    @MainThread
    protected abstract @ShouldFetch int shouldFetch(@Nullable ResultType data);

    // Called to get the cached data from the database
    @NonNull @MainThread
    protected abstract LiveData<ResultType> loadFromDb();

    // Called to create the API call.
    @NonNull @MainThread
    protected abstract List<LiveData<ApiResponse<RequestType>>> createCall();

    // Called when the fetch fails. The child class may want to reset components
    // like rate limiter.
    @MainThread
    protected abstract <T> void onFetchFailed(ApiResponse<T> response);

    private final MediatorLiveData<Resource<ResultType>> result = new MediatorLiveData<>();
    private RequestType requestResult;

    @MainThread
    public NetworkBoundResource() {
        result.setValue(Resource.notSet(null));
        LiveData<ResultType> dbSource = loadFromDb();
        result.addSource(dbSource, data -> {
            result.removeSource(dbSource);
            switch (shouldFetch(data)) {
                case SHOULD_NOT_FETCH:
                    result.addSource(dbSource,
                            newData -> result.setValue(Resource.success(newData)));
                    break;
                case SHOULD_FETCH_ALL:
                    fetchFromNetwork(SHOULD_FETCH_ALL,dbSource);
                    break;
                case SHOULD_FETCH_PASSAGES:
                    //supprimer passages
                    fetchFromNetwork(SHOULD_FETCH_PASSAGES,dbSource);
            }
        });
    }

    private void fetchFromNetwork(@ShouldFetch int typeFetch, final LiveData<ResultType> dbSource) {
        List<LiveData<ApiResponse<RequestType>>> apiResponses = createCall();
        switch (typeFetch) {
            case SHOULD_FETCH_ALL:
                result.addSource(dbSource,
                        newData -> result.setValue(Resource.loadingAll(newData)));
                break;
            case SHOULD_FETCH_PASSAGES:
                result.addSource(dbSource,
                        newData -> result.setValue(Resource.loadingPartially(newData)));
                break;
            case SHOULD_NOT_FETCH:
                break;
        }
        fetchList(dbSource,apiResponses);
    }

    private void fetchList(final LiveData<ResultType> dbSource, List<LiveData<ApiResponse<RequestType>>> apiResponses) {
        result.addSource(apiResponses.get(0), response -> {
            result.removeSource(apiResponses.get(0));
            result.removeSource(dbSource);
            //noinspection ConstantConditions
            if (response.isSuccessful()) {
                apiResponses.remove(0);
                if(requestResult == null)
                    requestResult = response.body;
                else {
                    if(response.body instanceof List)
                        ((List)requestResult).addAll((Collection) response.body);
                }
                if(!apiResponses.isEmpty())
                    fetchList(dbSource,apiResponses);
                else
                    saveResultAndReInit(requestResult);
            } else {
                onFetchFailed(response);
                result.addSource(dbSource,
                        newData -> result.setValue(
                                Resource.error(response.errorMessage, newData)));
            }
        });
    }

    @MainThread
    private void saveResultAndReInit(RequestType response) {
        new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... voids) {
                saveCallResult(response);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                // we specially request a new live data,
                // otherwise we will get immediately last cached value,
                // which may not be updated with latest results received from network.
                result.addSource(loadFromDb(),
                        newData -> result.setValue(Resource.success(newData)));
            }
        }.execute();
    }

    // returns a LiveData that represents the resource, implemented
    // in the base class.
    public final LiveData<Resource<ResultType>> getAsLiveData() {
        return result;
    }
}