package fr.sullygroup.metrowatch.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ImageView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import dagger.android.AndroidInjection;
import fr.sullygroup.metrowatch.BuildConfig;
import fr.sullygroup.metrowatch.Utils;
import fr.sullygroup.metrowatch.activities.MainActivity;
import fr.sullygroup.metrowatch.R;
import fr.sullygroup.metrowatch.model.Arret;
import fr.sullygroup.metrowatch.model.Arret_;
import fr.sullygroup.metrowatch.model.Passage;
import fr.sullygroup.metrowatch.model.StopLigne;
import fr.sullygroup.metrowatch.model.Ligne;
import fr.sullygroup.metrowatch.model.Ligne_;
import fr.sullygroup.metrowatch.model.Stop;
import fr.sullygroup.metrowatch.network.retrofit.WebService;
import io.objectbox.BoxStore;
import io.objectbox.android.AndroidObjectBrowser;
import okhttp3.internal.Util;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by jocelyn.caraman on 09/10/2017.
 */

public class SplashActivity extends SplashActivityBase implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener{
    private FusedLocationProviderClient mFusedLocationClient;

    final int REQUEST_FINE_LOCATION = 1;
    private LocationCallback mLocationCallback;
    private GoogleApiClient mGoogleApiClient;

    private boolean isRequestingLocationUpdates = false;
    private boolean isLastLocationKnown = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_FINE_LOCATION: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted
                    requestLocation();
                } else {
                    // permission denied
                }
                return;
            }
        }
    }

    void requestLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                // Show an explanation
                Log.d(TAG, "request permission");

            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        REQUEST_FINE_LOCATION);
            }
        } else {
            mFusedLocationClient.getLastLocation()
                    .addOnSuccessListener(this, location -> {
                        // Got last known location. In some rare situations this can be null.
                        if (location != null) {
                            Log.d(TAG, location.toString());
                            long timeFix = location.getTime();
                            long diff = Calendar.getInstance().getTimeInMillis() - timeFix;
                            if(diff <= 300){
                                setLastLocationKnown(true);
                                lat = location.getLatitude();
                                longi = location.getLongitude();
                            }
                            else
                                startLocationUpdates();
                        } else {
                            Log.d(TAG, "getLastLocation null");
                            startLocationUpdates();
                        }
                    });
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mFusedLocationClient != null && !isLastLocationKnown && !isRequestingLocationUpdates) {
            startLocationUpdates();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (isRequestingLocationUpdates) {
            stopLocationUpdates();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if(mGoogleApiClient != null)
            mGoogleApiClient.disconnect();
    }

    private void startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                Log.d(TAG, "show explanation");

            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        REQUEST_FINE_LOCATION);
            }
        } else {
            LocationRequest lr = LocationRequest.create()
                    .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                    .setInterval(10 * 1000)
                    .setFastestInterval(1 * 1000);
            Task t = mFusedLocationClient.requestLocationUpdates(lr,
                    mLocationCallback,
                    null /* Looper */);
            t.addOnSuccessListener(o -> Log.d(TAG,"startLocationUpdate"));
            isRequestingLocationUpdates = true;
        }

    }

    private void stopLocationUpdates() {
        mFusedLocationClient.removeLocationUpdates(mLocationCallback);
        isRequestingLocationUpdates = false;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.d(TAG,"connected");
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                Log.d(TAG,"onLocationResult");
                for (Location location : locationResult.getLocations()) {
                    Log.d(TAG,location.toString());
                    setLastLocationKnown(true);
                    lat = location.getLatitude();
                    longi = location.getLongitude();
                }
            }
        };

        requestLocation();
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.d(TAG, "onConnectionSuspended: " + i);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.e(TAG, "onConnectionFailed: " + connectionResult);
    }

    private void setLastLocationKnown(boolean lastLocationKnown) {
        isLastLocationKnown = lastLocationKnown;
        if(isLastLocationKnown && isRequestingLocationUpdates){
            stopLocationUpdates();
            getData();
        }
        if(canLaunchMainActivity())
            startMainActivity();
    }

    @Override
    protected boolean canLaunchMainActivity(){
        return(isDataReady && isLastLocationKnown);
    }
}
